function [llVal, dataTerms, q_h, q_m] = EM_E_step_ind( x_t, x_t_1, resStruc, garbageGrpFlag, qmFlag)
%EM_E_step_ind compute Estimation directly from an set of variables
%parameters:
%x_t: input at time t, size [N_f N_t]
%x_t_1: input at time t+1, size [N_f N_t_1]
%resStruc, model structure, consist of r, S_h, g_h, lpi, lga
%garbageGrpFlag, a flag either of 1 or 0.
fprintf( 'computing some useful values for later commputation...' );
x_t_1 = single(x_t_1);
x_t = single(x_t);
%yty = diag( x_t_1'*x_t_1 ); %[N_t_1 1]
tmp = zeros(size(x_t_1, 2), 1, 'single');
cTimes = ceil( size(x_t_1, 2) / 10000 );
cnt = 1;
for z = 1:cTimes
    if z ~= cTimes
        tmp(cnt:cnt+9999) = diag( x_t_1(:, cnt:cnt+9999)'*x_t_1(:, cnt:cnt+9999) );
    else
        tmp(cnt:end) = diag( x_t_1(:, cnt:end)'*x_t_1(:, cnt:end) );
    end
    cnt = cnt + 10000;
end
yty = tmp;
%xtx = diag( x_t'*x_t ); %[N_t 1]
tmp = zeros(size(x_t, 2), 1, 'single');
cTimes = ceil( size(x_t, 2) / 10000 );
cnt = 1;
for z = 1:cTimes
    if z ~= cTimes
        tmp(cnt:cnt+9999) = diag( x_t(:, cnt:cnt+9999)'*x_t(:, cnt:cnt+9999) );
    else
        tmp(cnt:end) = diag( x_t(:, cnt:end)'*x_t(:, cnt:end) );
    end
    cnt = cnt + 10000;
end
xtx = tmp;

% ytx = single( x_t_1'*x_t ); %[N_t_1 N_t]
fprintf( 'done\n' );
%% assignment
r = resStruc.r;
S_h = resStruc.S_h;
g_h = resStruc.g_h;
lpi = resStruc.lpi;
lga = resStruc.lga;
G = size(g_h, 2);
[Nf, N_t] = size(x_t);
[~, N_t_1] = size(x_t_1);
q_h = zeros(G, N_t_1, 'single');
if qmFlag == 1
    q_m = zeros(N_t, N_t_1, 'single');
else
    q_m = [];
end
S_c = r + S_h'; %[G 1]


ytySig = bsxfun( @times, yty, 1./S_c' ); %[N_t_1 G]
xtxSig = bsxfun( @times, xtx, 1./S_c' ); %[N_t, G]
gtg = single( diag( g_h'*g_h ).*(1./S_c) ); %[G 1]
ytg = single( x_t_1'*g_h );
ytg = bsxfun( @times, ytg, 1./S_c' ); %[N_t_1 G]
xtg = single( x_t'*g_h );
xtg = bsxfun( @times, xtg, 1./S_c' );%[N_t G]
%const1 = -log(2*pi)*(Nf/2);
cc = zeros( N_t, G, 'single' );
for i = 1:G
    cc(:, i) = -0.5*log( S_c(i) )*Nf;
end
%cc = 2*const1 + cc;
if ~isempty( find(cc==inf, 1) ) || ~isempty( find(cc==-inf, 1) ) || ...
        ~isempty( find( isnan(cc), 1 ) )
    fprintf( 'there is numerical error in log-determinat terms...\n' );
    cc(cc==inf) = -0.5*log(1e-100); %hacking...
end
pH0 = -0.5*xtxSig - xtg;
pH0 = bsxfun( @plus, pH0, -0.5*gtg' );
llVal = 0;
dataTerms = 0;
parfor i = 1:N_t_1
    %E-step
    pH1 = bsxfun( @plus, pH0, -0.5*ytySig(i, :) ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, ytg(i, :) ); %[N_t G]
%     pH2 = bsxfun( @times, single( ytx(i, :)' ), 1./S_c' ); %[N_t G]
    ytxVec = x_t_1(:, i)'*x_t;
    pH2 = bsxfun( @times, ytxVec', 1./S_c' );
    pH1 = pH1 + pH2 + cc;
    pH1 = bsxfun( @plus, pH1, lpi' ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, lga ); %[N_t G]
    cPosterior = pH1;
    mm = max(max( cPosterior, [], 1), [], 2);
    logP = cPosterior;
    cPosterior = cPosterior - mm;
%     test = gpuArray(cPosterior);
    eCP = exp(cPosterior);
%     eCP = gather(eCP);
    sumV = sum(sum(eCP, 1),2);
    llVal = llVal + log( sumV ) + mm ;
    cPosterior = eCP / sumV ;
    dataTerms = dataTerms + sum(sum(cPosterior.*logP));
    %M-step
    %for g_h
    if qmFlag == 1
        q_m(:, i) = sum( cPosterior, 2 );
    else
        q_m = [];
    end
    q_h(:, i) = sum( cPosterior, 1 );
end
if garbageGrpFlag == 1
    [~, tmp] = max( q_h, [], 1 );
    tmp = length(find( tmp == G ));
    tmpStr = sprintf( '\n\n *** number of points using garbageGrp: %d/%d. expected number of points using garbageGrp: %.3g \n\n', tmp, N_t_1, sum( q_h(end, :) ) );
    fprintf( tmpStr );
end
end
