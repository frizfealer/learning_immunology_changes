function [ resStrucCel, llCel, g_pri, S_pri, uCel, rVec, sVec, rho ] = EM_v1_1_5_7_ADMM( dataCell, G , gData, tmpName, params)
%EM_v1_1_5_7_ADMM EM version for our model. It is an exact EM without
%constraints. Speed and memory optimized. 
%This version using ADMM to have an hierarchical model. This program can be
%speed up by parpool in matlab
% Arguments:
%   dataCell, a cell of size [M 2], that are M experiments need to be done,
%   where each [M, 1] is time t and [M, 2] is time t+1.
%   G: # gradient groups
%   gData:, model parameters, the resStrucCel, plus the g_pri and S_pri from the previous
%   iterations. So g_pri and S_pri also needs to be the fields of gData. If
%   no, set it as [].
%   tmpName: temp file name saved for all output. If no, set it as [], the
%   program will output file name as tmp
%   params: a data structure of parameters. If no, set it as []. The fields
%   in it are:
%       SMALL_MEMORY_FLAG: default is on, to avoid computing/saving all
%       posterior prob.
%       OUT_FOLDER: the output folder for tmpFile, deafault is pwd
%       ITER_NUM: the iteration number of EM algorithm.
%       ADMM_IT_NUM: the iteration number of ADMM in M step.
%       garbageGrpFlag: default is on, add a gradient group of garbage
%       gradient.
%       gProb: the prior for the garbage group, usually is near to 1. 
%       Default value is 1-1e-14.
%       rVal: the measurement variance. Default is 1e-4.

% Returns:
%   resStrucCel: a result data structure for each exerpiment. There are m
%   experiments in input; so the length of resStrucCel = M. This data
%   structure has the following field:
%       g_h: the learned gradient for each gradient group. It is a N-by-G 
%       matrix, where N is # measurments nad G is #gradient groups
%       S_h: the learned variance for each gradient group.
%       lpi: the prior probability for each gradient group.
%       lga: the prior probability for each cell sample.
%       lpi and lga are set as uniform prior now.
%       r: the variance of measurment, currently just fixed as a small
%       value.
%       q_m: the posterior probability for each cell sample. it is a N-by-P
%       matrix, where N is the number of cell in time point t+1, and P is
%       the number of cells in time point t.
%       q_h: the posterior probability for each gradient. it is a N-by-G
%       matrix, where N is the number of cell in time point t+1.
%       if SMALL_MEMORY_FLAG is on q_m and q_h are empty.
%   llCel: log-likelihood for each experiment. Each cell corresponds to an
%   experiment. Inside every cell, a log-likelihood vector records the ll
%   at each iteration.
%   g_pri: the primary gradient learned from all data.
%   S_pri: the primary variance learned from all data.
%   The two above are the results from ADMM. They are analysis summary of
%   input
%   rVec, the ADMM result, it is the primal residual, it has two fields:
%   rVec.mu and rVec.var. It records the values of each iteration. for
%   mu and var it is a array of #iterations-by-G-by-M.
%   sVec, the ADMM result, it is the dual residual, other are the same as
%   rVec.
%   rho, the ADMM result, it also has two fields: mu and var.


%% self-setting parameters
params = checkParams(params);
SMALL_MEMORY_FLAG = params.SMALL_MEMORY_FLAG;
USE_GPU_FLAG = params.USE_GPU_FLAG;
OUT_FOLDER = params.OUT_FOLDER;
ITER_NUM = params.ITER_NUM;
ADMM_IT_NUM = params.ADMM_IT_NUM;
garbageGrpFlag = params.garbageGrpFlag;
gProb = params.gProb;
rVal = params.rVal;
expNum = size(dataCell, 1);
resStrucCel = cell( expNum, 1 );
%% initialization
Nf = size(dataCell{1, 1}, 2);
%init g_pri
if garbageGrpFlag == 1
    g_pri = zeros( Nf, G+2 );
    S_pri = zeros( 1, G+2 );
else
    g_pri = zeros( Nf, G+1 );
    S_pri = zeros( 1, G+1 );
end


%% if given gData (e.g. the results from the previous iterations)
if ~isempty(gData)
    for i = 1:expNum
        resStrucCel{i}.g_h = gData.resStrucCel{i}.g_h; 
        resStrucCel{i}.lga = gData.resStrucCel{i}.lga; 
        resStrucCel{i}.lpi = gData.resStrucCel{i}.lpi;
        resStrucCel{i}.S_h = gData.resStrucCel{i}.S_h;
        resStrucCel{i}.r = gData.resStrucCel{i}.r;
%         resStrucCel{i}.q_m = gData.resStrucCel{i}.q_m;
%         resStrucCel{i}.q_h = gData.resStrucCel{i}.q_h;
        g_pri = gData.g_pri;
        S_pri = gData.S_pri;
    end
else
    %init every values
    for i = 1:expNum
        [ resStrucCel{i} ] = ...
            EM_init( dataCell{i, 1}', dataCell{i, 2}', G, garbageGrpFlag, gProb, rVal, SMALL_MEMORY_FLAG );
        g_pri = g_pri + resStrucCel{i}.g_h;
        S_pri = S_pri + resStrucCel{i}.S_h;
    end
    g_pri = g_pri / expNum;
    S_pri = S_pri / expNum;
end


%[ ~, q_m_h ] = LL( log_p_m_h, lga, lpi );

%pre-computing the matrices for each experiments
llCel = cell( expNum, 1 );
yty = cell( expNum, 1 );
xtx = cell( expNum, 1 );
ytx = cell( expNum, 1 );
distCel = cell( expNum, 1 );
for i = 1:expNum
    tStart = tic;
    fprintf( 'computing useful values for (exp. %d):\t', i );
    llCel{i} = zeros(ITER_NUM, 1);
    x_t = single( dataCell{i, 1}' );
    x_t_1 = single( dataCell{i, 2}' );
    %yty{i} = single( diag( x_t_1'*x_t_1 ) ); %[N_t_1 1]
    tmp = zeros(size(x_t_1, 2), 1, 'single');
    cTimes = ceil( size(x_t_1, 2) / 10000 );
    cnt = 1;
    for z = 1:cTimes
        if z ~= cTimes
            tmp(cnt:cnt+9999) = diag( x_t_1(:, cnt:cnt+9999)'*x_t_1(:, cnt:cnt+9999) );
        else
            tmp(cnt:end) = diag( x_t_1(:, cnt:end)'*x_t_1(:, cnt:end) );
        end
        cnt = cnt + 10000;
    end
    yty{i} = tmp;
    %xtx{i} = single( diag( x_t'*x_t ) ); %[N_t 1]
    tmp = zeros(size(x_t, 2), 1, 'single');
    cTimes = ceil( size(x_t, 2) / 10000 );
    cnt = 1;
    for z = 1:cTimes
        if z ~= cTimes
            tmp(cnt:cnt+9999) = diag( x_t(:, cnt:cnt+9999)'*x_t(:, cnt:cnt+9999) );
        else
            tmp(cnt:end) = diag( x_t(:, cnt:end)'*x_t(:, cnt:end) );
        end
        cnt = cnt + 10000;
    end
    xtx{i} = tmp;
    if SMALL_MEMORY_FLAG == 1
        ytx{i} = [];
    else
        ytx{i} = single( x_t_1'*x_t ); %[N_t_1 N_t]
    end
    if SMALL_MEMORY_FLAG == 1
        distCel{i} = [];
    else
        distCel{i} = zeros( size(x_t_1, 2), size(x_t, 2), 'single' );
        for j = 1:size(x_t_1, 2)
            tmp = bsxfun( @minus, x_t_1(:, j), x_t );
            tmp = sqrt(sum(tmp.^2, 1)); 
            distCel{i}(j, :)= tmp;
        end
    end
    tElapsed = toc(tStart);
    fprintf( 'cost %.3g s.\n', tElapsed );
end

% if SHOW_FIGURES_FLAG == 1
%     qhSumVec = zeros(iter, 2);
%     figure; ca(1) = subplot(2, 2, 1); ca(2) = subplot(2, 2, 2); ca(3) = subplot(2, 2, 3); ca(4) = subplot(2, 2, 4);
%     pause;
%     figure; ca(5) = subplot(1, 1, 1);
% end

allLL = zeros(ITER_NUM, 1);
iter = 1;
while iter <= ITER_NUM
%     if iter > 100
%         ADMM_IT_NUM = round(1.5*ADMM_IT_NUM);
%     end
    ghNomCel = cell( expNum, 1 );
    ghDenomCel = cell( expNum, 1 );
    shNomCel = cell( expNum, 1 );
    shDenomCel = cell( expNum, 1 );
    qhSumCel = cell( expNum, 1 );
    qmSumCell = cell( expNum, 1 );
    fprintf( ['iteration = ' num2str(iter) '\n'] );
    for i = 1:expNum
        fprintf( 'expNum: %d\n', i );
        x_t = dataCell{i, 1}';
        x_t_1 = dataCell{i, 2}';
        N_t_1 = size(x_t_1, 2);

        %E step
%         [llVal, ghNom, ghDenom, shNom, shDenom, q_h, q_m] = ...
%             EM_E_step( x_t, x_t_1, yty{i}, xtx{i}, ytx{i}, resStrucCel{i}, garbageGrpFlag);
        if SMALL_MEMORY_FLAG == 0
                [llVal, ghNom, ghDenom, shNom, shDenom, q_h, q_m] = ...
            EM_E_step_pruning( x_t, x_t_1, yty{i}, xtx{i}, ytx{i}, resStrucCel{i}, garbageGrpFlag, distCel{i});
        else
            if USE_GPU_FLAG == 0
                [llVal, ghNom, ghDenom, shNom, shDenom, q_hSum, q_mSum] = ...
                EM_E_step_pruning_lm( x_t, x_t_1, yty{i}, xtx{i}, resStrucCel{i}, garbageGrpFlag, distCel{i});
            else
                [llVal, ghNom, ghDenom, shNom, shDenom, q_hSum, q_mSum] = ...
                EM_E_step_pruning_lm_GPU( single(x_t), single(x_t_1), yty{i}, xtx{i}, resStrucCel{i}, garbageGrpFlag);
            end
        end
%         [llVal, ghNom, ghDenom, shNom, shDenom, q_h, q_m] = ...
%             EM_E_step_speedup( x_t, x_t_1, yty{i}, xtx{i}, ytx{i}, resStrucCel{i}, garbageGrpFlag);
%         if SHOW_FIGURES_FLAG == 1 && i == 15
%             qhSumVec(iter, 1) = q_hSum(C_GRAD);
%             qhSumVec(iter, 2) = q_hSum(C_GRAD) / (resStrucCel{i}.S_h(C_GRAD)+resStrucCel{i}.r);
%             plot(ca(1), qhSumVec(1:iter, :) ); axes(ca(1)); 
%             title( sprintf( 'sum(q h)and sum(q_h)/(%.2g+1e-4), it = %d', resStrucCel{i}.S_h(C_GRAD), iter ) ); 
%             legend( 'qhSum', 'qhSum/var', 'Location', 'best' );
%             
%             tmp = zeros(size(g_pri, 1), 1);
%             tmp(:, 1) = rho(C_GRAD)*g_pri(:, C_GRAD);
%             tmp(:, 2) = uCel{i}(:, C_GRAD);
%             tmp(:, 3) = ghNom(:, C_GRAD) / (resStrucCel{i}.S_h(C_GRAD)+resStrucCel{i}.r);
%             tmp(:, 4) = qhSumVec(iter, 2);
%             axes(ca(2)); imagesc(tmp); colorbar; title( sprintf( 'rho = %d', rho(C_GRAD) ) );
%             set(gca, 'xtick', 1:4);
%             set(gca, 'xticklabel', {'\rho*g_pri', 'u', 'ghNom', 'ghDenom'} );
%             set(gca, 'xtickLabelRotation', 45 );
%             drawnow;
% %             pause;
%         end
        llCel{i}(iter) = llVal;
        if SMALL_MEMORY_FLAG == 0
            resStrucCel{i}.q_h = q_h;
            resStrucCel{i}.q_m = q_m;
            q_hSum = sum( q_h, 2 );
            q_mSum = sum( q_m, 2 );
        end
        ghNomCel{i} = ghNom; ghDenomCel{i} = ghDenom;
        shNomCel{i} = shNom; shDenomCel{i} = shDenom; 
        qhSumCel{i} = q_hSum; qmSumCell{i} = q_mSum;
%         usedGradMap(i, :) = 0;
%         tIdx =  q_hSum > RETAIN_GRP_TOL;
%         tIdx = 1:size(usedGradMap, 2);
%         usedGradMap(i, tIdx) = 1;
%             uCel{i} = updateU;
%             usedGradMap(i, :) = usedMap;
%         if i == 1 && iter >= 2
%             insVec( iter ) = q_hSum(1);
%             plot(ca(1), insVec(1:iter) );
%             insTmp(:, 1) = g_h(:, 1);
%             insTmp(:, 2) = uCel{1}(:, 1);
%             insTmp(:, 3) = g_pri(:, 1);
%             axes(ca(2)); imagesc(insTmp); colorbar;
%             drawnow;
%         end
%         if i == 2 && iter >= 2
%             insVec( iter ) = q_hSum(1);
%             plot(ca(3), insVec(1:iter) );
%             insTmp(:, 1) = g_h(:, 1);
%             insTmp(:, 2) = uCel{2}(:, 1);
%             insTmp(:, 3) = g_pri(:, 1);
%             axes(ca(4)); imagesc(insTmp); colorbar;
%             drawnow;
%         end
        %check first order difference
%         fDiffVec = llCel{i}(2:iter)-llCel{i}(1:iter-1);
%         fprintf( 'iter %d: LL = %g\n', iter, llCel{i}(iter) );
%         if iter > 1
%             if abs(llCel{i}(iter) - llCel{i}(iter - 1) )<TOL
%                 breakFlagVec(i) = 1;
%             end
%         end
%         if iter > 2
%             if abs( abs(fDiffVec(end))-abs(fDiffVec(end-1)) ) < TOL &&...
%                     fDiffVec(end)*fDiffVec(end-1) < 0
%                 breakFlagVec(i) = 1;
%             end
%         end
        if SMALL_MEMORY_FLAG == 1
            resStrucCel{i}.q_h = [];
            resStrucCel{i}.q_m = [];
        end
    end
    %ADMM on M step
    %init rVec.mu
    rVec.mu = zeros( ADMM_IT_NUM, size(g_pri, 2), expNum );
    rVec.var = zeros( ADMM_IT_NUM, size(S_pri, 2), expNum );
    %init sVec.mu
    sVec.mu = zeros( ADMM_IT_NUM, size(g_pri, 2) );
    sVec.var = zeros( ADMM_IT_NUM, size(S_pri, 2) );
    %init rho.mu, rho.var size: [1 G]
    rho.mu = ones( 1, size(g_pri, 2) );
    rho.var = ones( 1, size(g_pri, 2) )*1e-2;
    %init uCel.mu, rho.var
    uCel.mu = cell( expNum, 1 );
    uCel.var = cell( expNum, 1);
    for i = 1:expNum
        if garbageGrpFlag == 1
            uCel.mu{i} = zeros(Nf, G+2);
            uCel.var{i} = zeros(1, G+2);

        else
            uCel.mu{i} = zeros(Nf, G+1);
            uCel.var{i} = zeros(1, G+2);
        end
    end
    fprintf( 'M steup... ');
    for z = 1:ADMM_IT_NUM
        for i = 1:expNum
            ghNom = ghNomCel{i}; ghDenom = ghDenomCel{i};
            shNom = shNomCel{i}; shDenom = shDenomCel{i};
%             q_hSum = qhSumCel{i}; q_mSum = qmSumCell{i};
            [g_h, S_h, lpi, lga] = ...
                EM_M_step_ADMM( ghNom, ghDenom, shNom, shDenom,...
                g_pri, uCel.mu{i}, rho.mu, S_pri, uCel.var{i}, rho.var, garbageGrpFlag, resStrucCel{i} );
            resStrucCel{i}.g_h = g_h;
            resStrucCel{i}.S_h = S_h;
            resStrucCel{i}.lpi = lpi;
            resStrucCel{i}.lga = lga;
        end
%         fprintf( 'updating g_pri...' );
        g_pri = zeros( size(resStrucCel{1}.g_h) );
        for i = 1:expNum
            udivideRho = bsxfun( @rdivide, uCel.mu{i}, rho.mu );
            g_pri = g_pri + resStrucCel{i}.g_h + udivideRho;
        end
        g_pri = g_pri / expNum;
%         fprintf( 'updating S_pri...' ); %in precision matrix
        S_pri = zeros( size(resStrucCel{1}.S_h) );
        for i = 1:expNum
            udivideRho = bsxfun( @rdivide, uCel.var{i}, rho.var );
            S_pri = S_pri + 1./(resStrucCel{i}.S_h) + udivideRho;
        end
        S_pri = S_pri / expNum;
        %         if SHOW_FIGURES_FLAG == 1
        %             axes(ca(3));
        %             tmp = zeros(size(resStrucCel{1}.g_h, 1), expNum );
        %             for i = 1:expNum
        %                 udivideRho = uCel{i}(:, C_GRAD) / rho(C_GRAD);
        %                 tmp(:, i) = resStrucCel{i}.g_h(:, C_GRAD) + udivideRho;
        %             end
        %             tmp(:, expNum+1) = g_pri(:, C_GRAD); imagesc(tmp);
        % %             colorbar; drawnow; pause;
        %             axes(ca(4));
        %             plot( [resStrucCel{15}.g_h(:, C_GRAD) g_pri(:, C_GRAD)] ); title( 'updated g' );
        %             legend( 'g_h', 'g_pri', 'Location', 'best' );
        %             drawnow;
        % %             pause;
        %         end
%         fprintf( 'done. ' );
%         fprintf( 'updating u...' );
        for i = 1:expNum
            cDiff = resStrucCel{i}.g_h - g_pri;
            uTimesCDiff = bsxfun( @times, rho.mu, cDiff );
            uCel.mu{i} = uCel.mu{i} + uTimesCDiff;
            cDiff = 1./(resStrucCel{i}.S_h) - S_pri;
            uTimesCDiff = bsxfun( @times, rho.var, cDiff );
            uCel.var{i} = uCel.var{i} + uTimesCDiff;
        end
%         fprintf( 'done. ' );
        if z >= 2
            for j = 1:size(resStrucCel{i}.g_h, 2)
                for i = 1:expNum
                    rVec.mu(z, j, i) =  ...
                        norm( resStrucCel{i}.g_h(:, j) - g_pri(:, j) );
                    rVec.var(z, j, i) = ...
                        abs(1./(resStrucCel{i}.S_h(j)) - S_pri(j));
                end
            end
            for i = 1:size(g_pri, 2)
                sVec.mu(z, i) = ...
                    norm( rho.mu(i)*(g_pri(:, i)-g_pri_prev(:, i)) );
                sVec.var(z, i) = ...
                    rho.var(i)*abs(S_pri(i) - S_pri_prev(i));
            end
            reportVec.mu = zeros( length(rho.mu), 2 );
            reportVec.var = zeros( length(rho.var), 2 );
            if garbageGrpFlag == 1
                effectiveGrpNum = length(rho.mu) - 2;
            else
                effectiveGrpNum = length(rho.mu) - 1;
            end
            for i = 1:effectiveGrpNum %gradient group number
                if max(rVec.mu(z, i, :), [], 3) > 10*sVec.mu(z, i)
                    rho.mu(i) = rho.mu(i)*2;
                end
                if sVec.mu(z, i) > 10*max(rVec.mu(z, i, :), [], 3)
                    rho.mu(i) = rho.mu(i)/2;
                end
                if min(rVec.var(z, i, :), [], 3) > 10*sVec.var(z, i)
                    rho.var(i) = rho.var(i)*1;
                end
                if sVec.var(z, i) > 10*min(rVec.var(z, i, :), [], 3)
                    rho.var(i) = rho.var(i)/1;
                end
                reportVec.mu(i, :) = [max(rVec.mu(z, i, :), [], 3) sVec.mu(z, i)];
                reportVec.var(i, :) = [max(rVec.var(z, i, :), [], 3) sVec.var(z, i)];
            end
        end
        g_pri_prev = g_pri;
        S_pri_prev = S_pri;
        S_pri = 1./(S_pri); %back to variance

%         fprintf( 'iteration %d: (rho.mu, rho.var) = ', iter );
%         for i = 1:length(rho.mu)
%             fprintf( '(%.2g, %.2g)\n', rho.mu(i),rho.var(i) );
%         end
%         axes(aaa); bar(resStrucCel{1}.S_h); axes(bbb); bar(resStrucCel{2}.S_h); drawnow;
    end
    fprintf( 'done.\n' );
%     fprintf( '%.3g ', S_pri );
%     fprintf( '\n' );
%     fprintf( '%.3g ', rho.var );
%     fprintf( '\n' );
    
    if iter > 1 && mod( iter, 10 ) == 0 %save temp file every 10 iteration.
        fprintf( 'save tmp image...' );
        tStart = tic;
        if isempty( tmpName )
            fileName = 'tmp.mat';
        else
            fileName = tmpName;
        end
        save( [OUT_FOLDER filesep fileName], 'resStrucCel', 'llCel', 'g_pri', 'S_pri', 'uCel', 'rVec', 'sVec', 'rho', '-v7.3' );
        tEnd = toc(tStart);
        fprintf( 'costs %.3g s.\n', tEnd );
    end
    tmp = 0;
    for i = 1:expNum
        tmp = tmp + llCel{i}(iter);
    end
    allLL(iter) = tmp;
    fprintf( 'total likelihood = %.3g, ', tmp);
    fprintf( 'ADMM summary: \n')
    fprintf( 'primal/dual residual:\n' );
    for i = 1:length(rho.mu)
        fprintf( 'group %d:(mean: %.2g  %.2g)\t(var: %.2g  %.2g)\n', i, reportVec.mu(i, 1), reportVec.mu(i, 2), reportVec.var(i, 1), reportVec.var(i, 2)  );
    end
    fprintf( 'mean: max primal diff.: %.2g, solution diff.: %.2g\t variance: max primal diff.: %.2g, solution diff: %.2g\n', ...
        max(reportVec.mu(:, 1)), norm(g_pri(:)-g_pri_prev(:)), max(reportVec.var(:,1)), norm(S_pri(:)-1./S_pri_prev(:)) );
%     if isempty( find(breakFlagVec == 0, 1) )
%         break;
%     end
    if iter >= 2
        cllCel = 0; pllCel = 0;
        for i = 1:expNum
            cllCel = cllCel + llCel{i}(iter);
            pllCel = pllCel + llCel{i}(iter-1);
        end
%         if abs(cllCel - pllCel) < abs(cllCel)*TOL
%             break;
%         end
    end
    iter = iter + 1;
end

end

function [ resStruc ] = EM_init( x_t, x_t_1, G, garbageGrpFlag, gProb, rVal, SMALL_MEMORY_FLAG )
%EM_init a sub-function to initialize EM param.
%g_h, S_h, lpi, s_, lga

[Nf, N_t] = size(x_t);
[~, N_t_1] = size(x_t_1);
resStruc = [];
%init g_h
tmp = min(N_t, N_t_1);
if tmp < 1000;
    idx = randperm(tmp, tmp);
else
    idx = randperm(1000, 1000);
end
[ mapping, ~, ~ ] = greedyMapping( x_t_1(:, idx)', x_t(:, idx)', 0 );
absDiff = abs(x_t_1(:, idx) - x_t(:, idx(mapping(:,2))));
diff = x_t_1(:, idx) - x_t(:, idx(mapping(:,2)) );
rng('default');
if garbageGrpFlag == 0
    G = G + 1;
    g_h = repmat( min(absDiff,[],2), [1, G] ) + 0.1*randn(Nf, G);
    g_h(:, end) = zeros(Nf, 1); %zero-gradient group
else
    G = G + 2;
    g_h = repmat( min(absDiff,[],2), [1, G] ) + 0.1*randn(Nf, G);
    g_h(:, end-1) = zeros(Nf, 1); %zero-gradient group
    g_h(:,end) = zeros(Nf, 1); %garbarge-gradient group
end
%init S_h
tmp = max(diag(cov( diff' )));
%initialize to the 10 times of the shortest distance mapping and add noise.
rng('default');
S_h= 10*tmp + tmp*abs(randn(1, G));
%set the variance of garbage group to the 10 times of possible variances.
if garbageGrpFlag == 1
    ins = diff.^2; ins = sum(ins, 1) / Nf;
    S_h(end-1) = min(ins)*1.5; %zero-gradient group
    S_h(end) = max(S_h(1:end-1))*10; %garbage-gradient group
    fprintf( 'S_h_garbageGrp = %.3g, S_h_zeroGrp = %.3g\n', S_h(end), S_h(end-1) );
else
    ins = diff.^2; ins = sum(ins, 2) / Nf;
    S_h(end-1) = ins*1.5; %zero-gradient group
    fprintf( 'S_h_zeroGrp = %.3g\n', S_h(end) );
end
for i = 1:length(S_h)
    fprintf( '%d: %.2g\t',i, S_h(i))
end
%init lpi
fprintf( '\n' );
if garbageGrpFlag == 1
    if ~isempty(gProb)
        lpi = zeros(G, 1);
        lpi(end) = log(gProb);
        lpi(1:end-1) = ( log(1-gProb)-log(G-1) )*ones( G-1, 1 );
    else
        lpi = -log(G)*ones(G,1);
    end
else
    lpi = -log(G)*ones(G,1);
end
%init r
%     smallDist = inf;
%     for i = 1:N_t
%         ins = bsxfun(@minus, x_t, x_t(:, i) );
%         tmp = sum(ins.^2, 1); tmp(i) = inf;
%         dist = min( tmp );
%         if dist < smallDist
%             smallDist = dist;
%         end
%     end
if isempty(rVal)
    %     r = ones(N_t, 1)*(sqrt(smallDist)/3)^2;
    r = 0.0001; %its a hack, set it as small as possible.
else
    r = rVal;
end
fprintf( 'set r^2 = %.3g\n', r(1) );
%init lga
lga = -log(N_t)*ones(N_t,1);
resStruc.g_h = g_h;
resStruc.S_h = S_h;
resStruc.lpi = lpi;
resStruc.r = r;
resStruc.lga = lga;
%output variables
if SMALL_MEMORY_FLAG == 0
    resStruc.q_m = single( zeros(N_t, N_t_1) );
    resStruc.q_h = single( zeros( G, N_t_1 ) );
else
    resStruc.q_m = [];
    resStruc.q_h = [];
end
end

%The E function without ADMM.
function [llVal, ghNom, ghDenom, shNom, shDenom, q_h, q_m] = EM_E_step( x_t, x_t_1, yty, xtx, ytx, resStruc, garbageGrpFlag)
tic;
r = resStruc.r;
S_h = resStruc.S_h;
g_h = resStruc.g_h;
lpi = resStruc.lpi;
lga = resStruc.lga;
G = size(g_h, 2);
[Nf, N_t] = size(x_t);
[~, N_t_1] = size(x_t_1);
q_h = zeros(G, N_t_1);
q_m = zeros(N_t, N_t_1);
S_c = r + S_h'; %[G 1]


ytySig = bsxfun( @times, yty, 1./S_c' ); %[N_t_1 G]
xtxSig = bsxfun( @times, xtx, 1./S_c' ); %[N_t, G]

gtg = single( diag( g_h'*g_h ).*(1./S_c) ); %[G 1]
ytg = single( x_t_1'*g_h );
ytg = bsxfun( @times, ytg, 1./S_c' ); %[N_t_1 G]
xtg = single( x_t'*g_h );
xtg = bsxfun( @times, xtg, 1./S_c' );%[N_t G]
%const1 = -log(2*pi)*(Nf/2);
cc = zeros( N_t, G );
for i = 1:G
    cc(:, i) = -0.5*log( S_c(i) )*Nf;
end
%cc = 2*const1 + cc;
if ~isempty( find(cc==inf, 1) ) || ~isempty( find(cc==-inf, 1) ) || ...
        ~isempty( find( isnan(cc), 1 ) )
    fprintf( 'there is numerical error in log-determinat terms...\n' );
    cc(cc==inf) = -0.5*log(1e-100); %hacking...
end
pH0 = -0.5*xtxSig - xtg;
pH0 = bsxfun( @plus, pH0, -0.5*gtg' );
llVal = 0;
ghNom = zeros(Nf, G);
ghDenom = zeros(1, G);
shNom = zeros(1, G);
shDenom = zeros(1, G);

eCP = single( zeros(N_t, G) );
parfor i = 1:N_t_1
    %E-step
    pH1 = bsxfun( @plus, pH0, -0.5*ytySig(i, :) ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, ytg(i, :) ); %[N_t G]
    pH2 = bsxfun( @times, single( ytx(i, :)' ), 1./S_c' ); %[N_t G]
    pH1 = pH1 + pH2 + cc;
    pH1 = bsxfun( @plus, pH1, lpi' ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, lga ); %[N_t G]
    cPosterior = pH1;
    mm = max(max( cPosterior, [], 1), [], 2);
    cPosterior = cPosterior - mm;
    eCP = exp(cPosterior);
    sumV = sum(sum(eCP, 1),2);
    llVal = llVal + log( sumV ) + mm ;
    cPosterior = eCP / sumV ;
    %M-step
    %for g_h
    q_m(:, i) = sum( cPosterior, 2 );
    q_h(:, i) = sum( cPosterior, 1 );
    ghDenom = ghDenom + q_h(:, i)';
    pH1 = bsxfun( @minus, x_t_1(:, i), x_t ); %[Nf N_t]
    ghNom = ghNom + pH1*cPosterior;
    %for S_h
    shDenom = shDenom + Nf*q_h(:, i)';
    pH1 = sum( pH1.^2, 1 ); %[1 N_t]
    shNom = shNom + pH1*cPosterior;
end
if garbageGrpFlag == 1
    [~, tmp] = max( q_h, [], 1 );
    tmp = length(find( tmp == G ));
    tmpStr = sprintf( '*** number of points using garbageGrp: %d/%d. expected number of points using garbageGrp: %.3g \n', tmp, N_t_1, sum( q_h(end, :) ) );
    fprintf( tmpStr );
end
toc;
end
%The first speed-up E function, deprecated.
function [llVal, ghNom, ghDenom, shNom, shDenom, q_h, q_m] = EM_E_step_speedup( x_t, x_t_1, yty, xtx, ytx, resStruc, garbageGrpFlag)
tic;
CHUNK_SIZE = 10;

r = resStruc.r;
S_h = resStruc.S_h;
g_h = resStruc.g_h;
lpi = resStruc.lpi;
lga = resStruc.lga;
G = size(g_h, 2);
[Nf, N_t] = size(x_t);
[~, N_t_1] = size(x_t_1);
S_c = r + S_h'; %[G 1]

CHUNK_NUM = floor(N_t_1/CHUNK_SIZE);
CHUNK_IDX = cell( CHUNK_NUM+1, 1 );
for i = 1:CHUNK_NUM
    CHUNK_IDX{i} = ((i-1)*CHUNK_SIZE+1):(i*CHUNK_SIZE);
end
CHUNK_IDX{end} = (i*CHUNK_SIZE+1):N_t_1;
CHUNK_NUM = CHUNK_NUM + 1;

ytySig = bsxfun( @times, yty, 1./S_c' ); %[N_t_1 G]
xtxSig = bsxfun( @times, xtx, 1./S_c' ); %[N_t, G]

gtg = single( diag( g_h'*g_h ).*(1./S_c) ); %[G 1]
ytg = single( x_t_1'*g_h );
ytg = bsxfun( @times, ytg, 1./S_c' ); %[N_t_1 G]
xtg = single( x_t'*g_h );
xtg = bsxfun( @times, xtg, 1./S_c' );%[N_t G]
%const1 = -log(2*pi)*(Nf/2);
cc = zeros( N_t, G );
for i = 1:G
    cc(:, i) = -0.5*log( S_c(i) )*Nf;
end
%cc = 2*const1 + cc;
if ~isempty( find(cc==inf, 1) ) || ~isempty( find(cc==-inf, 1) ) || ...
        ~isempty( find( isnan(cc), 1 ) )
    fprintf( 'there is numerical error in log-determinat terms...\n' );
    cc(cc==inf) = -0.5*log(1e-100); %hacking...
end
pH0 = -0.5*xtxSig - xtg;
pH0 = bsxfun( @plus, pH0, -0.5*gtg' );
pH0 = reshape( pH0, [1 N_t G] );
llVal = 0;
ghNom = zeros(Nf, G);
ghDenom = zeros(1, G);
shNom = zeros(1, G);
shDenom = zeros(1, G);

q_mCel = cell(CHUNK_NUM, 1);
q_hCel = cell(CHUNK_NUM, 1);

CHUNK_SIZE_VEC = ones(CHUNK_NUM, 1)*CHUNK_SIZE;
CHUNK_SIZE_VEC(end) = length(CHUNK_IDX{end});

parfor i = 1:CHUNK_NUM
    CHUNK_SIZE = CHUNK_SIZE_VEC(i);
    %E-step
    pH1 = bsxfun( @plus, pH0, reshape( -0.5*ytySig(CHUNK_IDX{i}, :), [CHUNK_SIZE 1 G] ) ); %[CS N_t G]
    pH1 = bsxfun( @plus, pH1, reshape( ytg(CHUNK_IDX{i}, :), [CHUNK_SIZE 1 G] ) ); %[CS N_t G]
    pH2 = bsxfun( @times, reshape( ytx(CHUNK_IDX{i}, :), [CHUNK_SIZE N_t 1] ), reshape( 1./S_c', [1 1 G] ) ); %[CS N_t G]
    pH1 = bsxfun( @plus, pH1 + pH2, reshape(cc, [1 N_t G]) ); %[CS N_t G]
    pH1 = bsxfun( @plus, pH1, reshape( lpi', [1 1 G] ) ); %[CS N_t G]
    pH1 = bsxfun( @plus, pH1, reshape( lga, [1 N_t 1] ) ); %[CS N_t G]
    cPosterior = pH1;
    mm = max(max( cPosterior, [], 2), [], 3); %[CS 1 1]
    cPosterior = bsxfun( @minus, cPosterior, mm ); %[CS N_t G]
    eCP = exp(cPosterior);
    sumV = sum(sum(eCP, 2),3); %[CS 1 1]
    llVal = llVal + sum(log( sumV ) + mm) ;
    cPosterior = bsxfun( @times, eCP, 1./sumV ); %[CS N_t G]
    %M-step
    %for g_h
    q_mCel{i} = sum( cPosterior, 3 ); %[CS N_t 1]
    q_hCel{i} = sum( cPosterior, 2 ); %[CS 1 G]
    ghDenom = ghDenom + reshape( sum( q_hCel{i}, 1 ), [1 G] ); %[1 G]
    pH1 = bsxfun( @minus, x_t_1(:, CHUNK_IDX{i}), reshape( x_t, [Nf 1 N_t] ) ); %[Nf CS N_t]
    oPH1 = pH1;
    pH1 = reshape( pH1, [Nf*CHUNK_SIZE N_t] ); %[Nf*CHUNK_SIZE N_t]
    tmpIdx = repmat( Nf, 1, CHUNK_SIZE );
    pH1Cel = mat2cell(pH1, tmpIdx, N_t)'; %an cell with CHUNK_NUM element, every element of size [Nf N_t]
    cPosterior = permute( cPosterior, [2 3 1] ); %[N_t G CS];
    cPosterior = reshape( cPosterior, [N_t G*CHUNK_SIZE] ); %[N_t G*CS]; 
    tmpIdx = repmat( G, 1, CHUNK_SIZE );
    cPosteriorCel = mat2cell(cPosterior, N_t, tmpIdx);
    tmp = cellfun( @(x,y)(x*y), pH1Cel, cPosteriorCel, 'UniformOutput', 0); %an cell with CHUNK_NUM element, every element of size [Nf G]
    tmp = cat(3, tmp{:}); %a array of dim [Nf G CS];
    tmp = sum(tmp, 3);
    ghNom = ghNom + tmp; %[Nf G];
    %for S_h
    shDenom = shDenom + Nf*reshape( sum( q_hCel{i}, 1 ), [1 G] ); %[1 G]
    pH1 = sum( oPH1.^2, 1 ); %[1 CS N_t] 
    pH1 = reshape(pH1, [CHUNK_SIZE N_t] ); %[CS N_t]
    tmpIdx = ones( 1, CHUNK_SIZE );
    pH1Cel = mat2cell(pH1, tmpIdx, N_t)'; %an cell with CHUNK_NUM element, every element of size [1 N_t]
    tmp = cellfun( @(x,y)(x*y), pH1Cel, cPosteriorCel, 'UniformOutput', 0); %an cell with CHUNK_NUM element, every element of size [1 G]
    tmp = cat(3, tmp{:}); %a array of dim [1 G CS];
    tmp = sum(tmp, 3); %[1 G];
    shNom = shNom + tmp;
end
q_h = cell2mat(q_hCel); 
q_h = reshape( q_h, [N_t_1 G] )';
q_m = cell2mat(q_mCel)';
if garbageGrpFlag == 1
    [~, tmp] = max( q_h, [], 1 );
    tmp = length(find( tmp == G ));
    tmpStr = sprintf( '\n\n *** number of points using garbageGrp: %d/%d. expected number of points using garbageGrp: %.3g \n\n', tmp, N_t_1, sum( q_h(end, :) ) );
    fprintf( tmpStr );
end
toc;
end
%The second speed-up E function by not computing every cells
%probabilities. It usually cost too much memory because q_m is a N_t*N_t_1
%matrix, and we don't use it.
function [llVal, ghNom, ghDenom, shNom, shDenom, q_h, q_m] = EM_E_step_pruning( x_t, x_t_1, yty, xtx, ytx, resStruc, garbageGrpFlag, distMat)
tStart = tic;
DIST_THRES = 1;
r = resStruc.r;
S_h = resStruc.S_h;
g_h = resStruc.g_h;
lpi = resStruc.lpi;
lga = resStruc.lga;
G = size(g_h, 2);
[Nf, N_t] = size(x_t);
[~, N_t_1] = size(x_t_1);
q_h = single( zeros(G, N_t_1) );
S_c = r + S_h'; %[G 1]


ytySig = bsxfun( @times, yty, 1./S_c' ); %[N_t_1 G]
xtxSig = bsxfun( @times, xtx, 1./S_c' ); %[N_t, G]

gtg = single( diag( g_h'*g_h ).*(1./S_c) ); %[G 1]
ytg = single( x_t_1'*g_h );
ytg = single( bsxfun( @times, ytg, 1./S_c' ) ); %[N_t_1 G]
xtg = single( x_t'*g_h );
xtg = single( bsxfun( @times, xtg, 1./S_c' ) );%[N_t G]
%const1 = -log(2*pi)*(Nf/2);
cc = zeros( N_t, G );
for i = 1:G
    cc(:, i) = -0.5*log( S_c(i) )*Nf;
end
%cc = 2*const1 + cc;
if ~isempty( find(cc==inf, 1) ) || ~isempty( find(cc==-inf, 1) ) || ...
        ~isempty( find( isnan(cc), 1 ) )
    fprintf( 'there is numerical error in log-determinat terms...\n' );
    cc(cc==inf) = -0.5*log(1e-100); %hacking...
end
pH0 = -0.5*xtxSig - xtg;
pH0 = bsxfun( @plus, pH0, -0.5*gtg' );
llVal = 0;
ghNom = zeros(Nf, G);
ghDenom = zeros(1, G);
shNom = zeros(1, G);
shDenom = zeros(1, G);

tmp = bsxfun( @plus, g_h, sqrt(S_h)*DIST_THRES );
gDist = sqrt(sum(tmp.^2, 1));
gDist = max(gDist(1:end-2));

q_mCel = cell(N_t_1, 1);
parfor i = 1:N_t_1
    %set points we want to compute
    tIdx =  distMat(i, :) < gDist ;
    if length(find(tIdx==1)) < 10
%         fprintf( '%d: %d\n', i, length(find(tIdx==1)) );
        tmp = bsxfun( @plus, g_h, sqrt(S_h)*(DIST_THRES+1) );
        gDist2 = sqrt(sum(tmp.^2, 1));
        gDist2 = max(gDist2(1:end-2));
        tIdx =  distMat(i, :) < gDist2 ;
    end
    %E-step
    pH1 = bsxfun( @plus, pH0(tIdx, :), -0.5*ytySig(i, :) ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, ytg(i, :) ); %[N_t G]
    pH2 = bsxfun( @times, single( ytx(i, tIdx)' ), 1./S_c' ); %[N_t G]
    pH1 = pH1 + pH2 + cc(tIdx, :);
    pH1 = bsxfun( @plus, pH1, lpi' ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, lga(tIdx) ); %[N_t G]
    cPosterior = pH1;
    mm = max(max( cPosterior, [], 1), [], 2);
    if ~isscalar(mm)
        cPosterior = single( zeros( N_t, G ) );
        cPosterior(:, end) = 1/N_t;
        tIdx = logical(1:N_t);
    else
        cPosterior = cPosterior - mm;
        cPosterior = exp(cPosterior);
        sumV = sum(sum(cPosterior, 1),2);
        llVal = llVal + log( sumV ) + mm ;
        cPosterior = cPosterior / sumV ;
    end
    %M-step
    %for g_h
    q_mVec = single( zeros(N_t, 1) );
%     if length(sum( cPosterior, 2 )) ~= length(find(tIdx==1))
%         keyboard;
%     end
    q_mVec(tIdx) = sum( cPosterior, 2 );
    q_mCel{i} = q_mVec;
    q_h(:, i) = sum( cPosterior, 1 );
    ghDenom = ghDenom + q_h(:, i)';
    pH1 = bsxfun( @minus, x_t_1(:, i), x_t(:, tIdx) ); %[Nf N_t]
    ghNom = ghNom + pH1*cPosterior;
    %for S_h
    shDenom = shDenom + Nf*q_h(:, i)';
    pH1 = sum( pH1.^2, 1 ); %[1 N_t]
    shNom = shNom + pH1*cPosterior;
end
q_m = cell2mat(q_mCel');
if garbageGrpFlag == 1
    [~, tmp] = max( q_h, [], 1 );
    tmp = length(find( tmp == G ));
    tmpStr = sprintf( '*** number of points using garbageGrp: %d/%d. expected number of points using garbageGrp: %.3g\n',...
        tmp, N_t_1, sum( q_h(end, :) ) );
    fprintf( tmpStr );
end
tEnd = toc(tStart);
fprintf( 'function "EM_E_step_pruning" cost %.3g s.\n', tEnd );
end
%The second speed-up E function by not computing every cells
%probabilities. It is the same as the previous function except we don't
%store posterior when computing this function.
function [llVal, ghNom, ghDenom, shNom, shDenom, q_hSum, q_mSum] = EM_E_step_pruning_lm( x_t, x_t_1, yty, xtx, resStruc, garbageGrpFlag, distMat)
%EM_E_step_pruning_lm EM_E_step_pruning less memory version
tStart = tic;
DIST_THRES = 2;
r = resStruc.r;
S_h = resStruc.S_h;
g_h = resStruc.g_h;
lpi = resStruc.lpi;
lga = resStruc.lga;
G = size(g_h, 2);
[Nf, N_t] = size(x_t);
[~, N_t_1] = size(x_t_1);
S_c = r + S_h'; %[G 1]


ytySig = bsxfun( @times, yty, 1./S_c' ); %[N_t_1 G]
xtxSig = bsxfun( @times, xtx, 1./S_c' ); %[N_t, G]

gtg = single( diag( g_h'*g_h ).*(1./S_c) ); %[G 1]
ytg = single( x_t_1'*g_h );
ytg = single( bsxfun( @times, ytg, 1./S_c' ) ); %[N_t_1 G]
xtg = single( x_t'*g_h );
xtg = single( bsxfun( @times, xtg, 1./S_c' ) );%[N_t G]
%const1 = -log(2*pi)*(Nf/2);
cc = zeros( N_t, G );
for i = 1:G
    cc(:, i) = -0.5*log( S_c(i) )*Nf;
end
%cc = 2*const1 + cc;
if ~isempty( find(cc==inf, 1) ) || ~isempty( find(cc==-inf, 1) ) || ...
        ~isempty( find( isnan(cc), 1 ) )
    fprintf( '[warning] there is numerical error in log-determinat terms...\n' );
    cc(cc==inf) = -0.5*log(1e-100); %hacking...
end
pH0 = -0.5*xtxSig - xtg;
pH0 = bsxfun( @plus, pH0, -0.5*gtg' );
llVal = 0;
ghNom = zeros(Nf, G);
ghDenom = zeros(1, G);
shNom = zeros(1, G);
shDenom = zeros(1, G);

tmp = bsxfun( @plus, g_h, sqrt(S_h)*DIST_THRES );
gDist = sqrt(sum(tmp.^2, 1));
gDist = max(gDist(1:end-2));

q_mSum = single( zeros(N_t, 1) );
q_hSum = single( zeros(G, 1) );
garbageGrpCnt = 0;
expGarbageGrpCnt = 0;
parfor i = 1:N_t_1
    %set points we want to compute
    %tIdx =  distMat(i, :) < gDist ;
    cDist = sqrt(sum(bsxfun( @minus, x_t_1(:, i),  x_t ).^2, 1));
    tIdx = cDist < gDist;
    if length(find(tIdx==1)) < 10
%         fprintf( '%d: %d\n', i, length(find(tIdx==1)) );
        tmp = bsxfun( @plus, g_h, sqrt(S_h)*(DIST_THRES+1) );
        gDist2 = sqrt(sum(tmp.^2, 1));
        gDist2 = max(gDist2(1:end-2));
        tIdx =  cDist < gDist2 ;
    end
    %E-step
    pH1 = bsxfun( @plus, pH0(tIdx, :), -0.5*ytySig(i, :) ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, ytg(i, :) ); %[N_t G]
    ytxVec = x_t_1(:, i)'*x_t(:, tIdx);
%     pH2 = bsxfun( @times, single( ytx(i, tIdx)' ), 1./S_c' ); %[N_t G]
    pH2 = bsxfun( @times, ytxVec', 1./S_c' );
    pH1 = pH1 + pH2 + cc(tIdx, :);
    pH1 = bsxfun( @plus, pH1, lpi' ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, lga(tIdx) ); %[N_t G]
    cPosterior = pH1;
    mm = max(max( cPosterior, [], 1), [], 2);
    if ~isscalar(mm)
        cPosterior = single( zeros( N_t, G ) );
        cPosterior(:, end) = 1/N_t;
        tIdx = logical(1:N_t);
    else
        cPosterior = cPosterior - mm;
        cPosterior = exp(cPosterior);
        sumV = sum(sum(cPosterior, 1),2);
        llVal = llVal + log( sumV ) + mm ;
        cPosterior = cPosterior / sumV ;
    end
    %M-step
    %for g_h
    q_mVec = single( zeros(N_t, 1) );
%     if length(sum( cPosterior, 2 )) ~= length(find(tIdx==1))
%         keyboard;
%     end
    q_mVec(tIdx) = sum( cPosterior, 2 );
    q_mSum = q_mSum + q_mVec;
    q_hVec = sum( cPosterior, 1 );
    q_hSum = q_hSum + q_hVec';
    ghDenom = ghDenom + q_hVec;
    pH1 = bsxfun( @minus, x_t_1(:, i), x_t(:, tIdx) ); %[Nf N_t]
    ghNom = ghNom + pH1*cPosterior;
    %for S_h
    shDenom = shDenom + Nf*q_hVec;
    pH1 = sum( pH1.^2, 1 ); %[1 N_t]
    shNom = shNom + pH1*cPosterior;
    
    if garbageGrpFlag == 1
        [~,idx] = max( q_hVec );
        if idx == G
            garbageGrpCnt = garbageGrpCnt + 1;
        end
        expGarbageGrpCnt = expGarbageGrpCnt + q_hVec(end);
    end
    
end
if garbageGrpFlag == 1
    tmpStr = sprintf( '*** number of points using garbageGrp: %d/%d. expected number of points using garbageGrp: %.3g ***\n',...
        garbageGrpCnt, N_t_1, expGarbageGrpCnt );
    fprintf( tmpStr );
end
tEnd = toc(tStart);
fprintf( 'function "EM_E_step_pruning_lm" cost %.3g s.\n', tEnd );
end

function [llVal, ghNom, ghDenom, shNom, shDenom, q_hSum, q_mSum] = EM_E_step_pruning_lm_GPU( x_t, x_t_1, yty, xtx, resStruc, garbageGrpFlag)
%EM_E_step_pruning_lm EM_E_step_pruning less memory version
tStart = tic;
r = resStruc.r;
S_h = resStruc.S_h;
g_h = resStruc.g_h;
lpi = resStruc.lpi;
lga = resStruc.lga;
lpi = gpuArray(lpi);
lga = gpuArray(lga);
G = size(g_h, 2);
[Nf, N_t] = size(x_t);
[~, N_t_1] = size(x_t_1);
S_c = r + S_h'; %[G 1]


ytySig = bsxfun( @times, yty, 1./S_c' ); %[N_t_1 G]
xtxSig = bsxfun( @times, xtx, 1./S_c' ); %[N_t, G]

gtg = single( diag( g_h'*g_h ).*(1./S_c) ); %[G 1]
ytg = single( x_t_1'*g_h );
ytg = single( bsxfun( @times, ytg, 1./S_c' ) ); %[N_t_1 G]
xtg = single( x_t'*g_h );
xtg = single( bsxfun( @times, xtg, 1./S_c' ) );%[N_t G]
%const1 = -log(2*pi)*(Nf/2);
cc = gpuArray( zeros( N_t, G, 'single' ) );
for i = 1:G
    cc(:, i) = -0.5*log( S_c(i) )*Nf;
end
%cc = 2*const1 + cc;
if ~isempty( find(cc==inf, 1) ) || ~isempty( find(cc==-inf, 1) ) || ...
        ~isempty( find( isnan(cc), 1 ) )
    fprintf( '[warning] there is numerical error in log-determinat terms...\n' );
    cc(cc==inf) = -0.5*log(1e-100);
end
pH0 = -0.5*xtxSig - xtg;
pH0 = bsxfun( @plus, pH0, -0.5*gtg' );
pH0 = gpuArray(pH0);
ytySig = gpuArray(ytySig);
ytg = gpuArray(ytg);
llVal = 0;
ghNom = gpuArray( zeros(Nf, G, 'single' ) );
ghDenom = gpuArray( zeros(1, G, 'single') );
shNom = gpuArray( zeros(1, G, 'single') );
shDenom = gpuArray( zeros(1, G, 'single') );


q_mSum = single( zeros(N_t, 1) );
q_hSum = single( zeros(G, 1) );
garbageGrpCnt = 0;
expGarbageGrpCnt = 0;

x_t_gpu = gpuArray( x_t(:, :) );
x_t_1_gpu = gpuArray( x_t_1(:, :) );
for i = 1:N_t_1
    %E-step
    pH1 = bsxfun( @plus, pH0(:, :), -0.5*ytySig(i, :) ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, ytg(i, :) ); %[N_t G]
    ytxVec = x_t_1_gpu(:, i)'*x_t_gpu;
%     pH2 = bsxfun( @times, single( ytx(i, tIdx)' ), 1./S_c' ); %[N_t G]
    pH2 = bsxfun( @times, ytxVec', 1./S_c' );
    pH1 = pH1 + pH2 + cc(:, :);
    pH1 = bsxfun( @plus, pH1, lpi' ); %[N_t G]
    pH1 = bsxfun( @plus, pH1, lga ); %[N_t G]
    cPosterior = pH1;
    mm = max(max( cPosterior, [], 1), [], 2);
    cPosterior = cPosterior - mm;
    cPosterior = exp(cPosterior);
    sumV = sum(sum(cPosterior, 1),2);
    llVal = llVal + log( sumV ) + mm ;
    cPosterior = cPosterior / sumV ;
    %M-step
    %for g_h
    q_mVec = single( zeros(N_t, 1) );
%     if length(sum( cPosterior, 2 )) ~= length(find(tIdx==1))
%         keyboard;
%     end
    q_mVec = sum( cPosterior, 2 );
    q_mSum = q_mSum + q_mVec;
    q_hVec = sum( cPosterior, 1 );
    q_hSum = q_hSum + q_hVec';
    ghDenom = ghDenom + q_hVec;
    pH1 = bsxfun( @minus, x_t_1_gpu(:, i), x_t_gpu ); %[Nf N_t]
    ghNom = ghNom + pH1*cPosterior;
    %for S_h
    shDenom = shDenom + Nf*q_hVec;
    pH1 = sum( pH1.^2, 1 ); %[1 N_t]
    shNom = shNom + pH1*cPosterior;
    
    if garbageGrpFlag == 1
        [~,idx] = max( q_hVec );
        if idx == G
            garbageGrpCnt = garbageGrpCnt + 1;
        end
        expGarbageGrpCnt = expGarbageGrpCnt + q_hVec(end);
    end
    
end
if garbageGrpFlag == 1
    tmpStr = sprintf( '*** number of points using garbageGrp: %d/%d. expected number of points using garbageGrp: %.3g ***\n',...
        garbageGrpCnt, N_t_1, expGarbageGrpCnt );
    fprintf( tmpStr );
end
tEnd = toc(tStart);
fprintf( 'function "EM_E_step_pruning_lm" cost %.3g s.\n', tEnd );
end

%The M function with ADMM
function [g_h, S_h, lpi, lga] = EM_M_step_ADMM( ghNom, ghDenom, shNom, shDenom, g_pri, uMat_g, rho_g, S_pri, uMat_S, rho_S, garbageGrpFlag, resStruc )
tStart = tic;
%ghNom: size [N_f G]
%ghDenom: size [1 G]
%g_pri: size [N_f G]
%uMat_g: size [N_f G]
%uMat_S: size [1 G]
%rho_g: size [1 G]
%rho_S: size [1 G]
S_h = resStruc.S_h; %size [1 G]
r = resStruc.r; %size scalar
lpi = resStruc.lpi; %size [G 1]
lga = resStruc.lga;
[Nf, G] = size( g_pri );
oGhNum = ghNom;
ghNom = bsxfun( @times, ghNom, 1./(S_h+r) );
ghDenom = ghDenom.*(1./(S_h+r));
g_h_woADMM = bsxfun( @times, ghNom+1e-4, 1./(ghDenom+1e-4) );
rhoTimesG_pri = bsxfun( @times, rho_g, g_pri );
if garbageGrpFlag == 1
    %for the graident group that is not used in the data, set uMat to zero
    g_h = bsxfun( @times, rhoTimesG_pri - uMat_g + ghNom, 1./( rho_g + ghDenom ) );
    g_h(:, end-1:end) = 0; %for zero-gradient and garbage gradient grp.
    oSh = S_h(end);
    p_S_h_4 = -(shDenom);
    p_S_h_2 = shNom + shDenom.*sum(g_h.^2, 1)/Nf - 2*sum(oGhNum.*g_h,1)...
        + 2*uMat_S - 2*rho_S/(S_pri);
    for i = 1:G-2
        par = [p_S_h_4(i) p_S_h_2(i) 2*rho_S(i)];
%         if ~isempty(find(isnan(par))) || ~isempty(find(par==inf, 1))
%             keyboard;
%         end
        tmp = roots(par);
        tmp = tmp(tmp>=0);
        if isempty(tmp)
            tmp = 0;
        end
        S_h(i) = max(tmp) -r(1);
    end
    idx = find( S_h < 0, 1 );
    if ~isempty(idx)
%         fprintf( '[warning] variance is negative value.\n' );
    end
    idx = S_h < 0 | isnan(S_h) ;
    S_h(idx) = 1e-4;
    S_h(end) = oSh; %for garbage gradient grp.
%     S_h(end-1) = min(S_h(1:end-2))*0.1;
    S_h(end-1) = 1e-4;
else
    %for the graident group that is not used in the data, set uMat to zero
    g_h = bsxfun( @times, rhoTimesG_pri - uMat_g + ghNom, 1./( rho_g + ghDenom ) );
    g_h(:, end) = 0; %zero-gradient group
    p_S_h_4 = -(shDenom);
    p_S_h_2 = shNom + shDenom.*sum(g_h.^2, 1)/Nf - 2*sum(oGhNum.*g_h,1)...
        + 2*uMat_S - 2*rho_S/(S_pri);
    for i = 1:G-1
        tmp = roots([p_S_h_4(i) p_S_h_2(i) 2*rho_S]);
        tmp = tmp(tmp>=0);
        if ~ismempty(tmp)
            S_h(i) = max(tmp) -r(1);
        else
            S_h(i) = 1e-4;
        end
    end
    idx = find( S_h < 0, 1 );
    if ~isempty(idx)
%         fprintf( '[warning] variance is negative value.' );
    end
    idx =  S_h < 0 | isnan(S_h) ;
    S_h(idx) = 1e-4;
%     S_h(end) = min(S_h(1:end-1))*0.1; %zero-gradient group
    S_h(end) = 1e-4;
end
if ( garbageGrpFlag == 1 && ~isempty( find(S_h(1:end-2) <=1e-6, 1) ) ) || ...
        ( garbageGrpFlag == 0 && ~isempty( find(S_h(1:end-1) <=1e-6, 1) ) )
    fprintf( 'there are groups'' variance <= 1e-6... they are:\t' );
    cIdx = find(S_h<=1e-6);
    if garbageGrpFlag == 1
        cIdx(cIdx==G-1) = []; %remove zero-gradient group
    else
        cIdx(cIdx==G) = [];
    end
    for i = 1:length(cIdx)
        fprintf( '(%d: %.2g) ',cIdx(i), S_h(cIdx(i)) );
    end
    fprintf( '\n' );
    S_h(cIdx) = 1e-6;
end
if garbageGrpFlag == 1
%     tmp = lpi(end);
%     lpi(1:end-1) = log( q_hSum(1:end-1)+1e-6 ) - log( sum(q_hSum(1:end-1)) ) + log(1-exp(tmp));
else
%     lpi = log( q_hSum+1e-6 ) - log( N_t_1 );
end
% lga = log( q_mSum+1e-16 ) - log(N_t_1);
tEnd = toc(tStart);
% fprintf( 'function "EM_M_step_ADMM" costs %.3g s\n', tEnd );
end
%The M function without ADMM.
function [g_h, S_h, lpi, lga] = EM_M_step( ghNom, ghDenom, shNom, shDenom, g_pri, garbageGrpFlag, resStruc )
tic;
%ghNom: size [N_f G]
%ghDenom: size [1 G]
%g_pri: size [N_f G]
%uMat: size [N_f G]
S_h = resStruc.S_h; %size [1 G]
r = resStruc.r; %size scalar
q_h = resStruc.q_h; %size [G N_t_1]
q_m = resStruc.q_m; %size [N_t N_t_1]
lpi = resStruc.lpi; %size [G 1]
[Nf, G] = size( g_pri );
N_t_1 = size(q_h, 2);
ghNom = bsxfun( @times, ghNom, 1./(S_h+r) );
ghDenom = ghDenom.*(1./(S_h+r));
if garbageGrpFlag == 1
    g_h = bsxfun( @times, ghNom, 1./ghDenom );
    g_h(:, end-1:end) = 0; %for zero-gradient and garbage gradient grp.
    oSh = S_h(end);
    S_h = shNom ./ shDenom - sum(g_h.^2, 1) / Nf -r(1);
    idx = find( S_h < 0, 1 );
    if ~isempty(idx)
        fprintf( '\n\n warning: variance is negative value\n\n.' );
    end
    idx = S_h < 0 | isnan(S_h) ; S_h(idx) = 1e-16; %a hack! be careful!
    S_h(end) = oSh; %for zero-gradient and garbage gradient grp.
    S_h(end-1) = min(S_h(1:end-2))*0.1;
else
    g_h = bsxfun( @times, ghNom, 1./ghDenom );
    g_h(:, end) = 0; %zero-gradient group
    S_h = shNom ./ shDenom - sum(g_h.^2, 1) / Nf - r(1);
    idx = find( S_h < 0, 1 );
    if ~isempty(idx)
        fprintf( '\n\n warning: variance is negative value\n\n.' );
    end
    idx =  S_h < 0 | isnan(S_h) ;
    S_h(idx) = 1e-16;
    S_h(end) = min(S_h(1:end-1))*0.1; %zero-gradient group
end
if ( garbageGrpFlag == 1 && ~isempty( find(S_h(1:end-2) <=1e-6, 1) ) ) || ...
        ( garbageGrpFlag == 0 && ~isempty( find(S_h(1:end-1) <=1e-6, 1) ) )
    fprintf( 'there is a group''s variance <= 1e-6... they are:\n' );
    cIdx = find(S_h<=1e-6);
    if garbageGrpFlag == 1
        cIdx(cIdx==G-1) = []; %remove zero-gradient group
    else
        cIdx(cIdx==G) = [];
    end
    for i = 1:length(cIdx)
        fprintf( '%d: %.2g\t',cIdx(i), S_h(cIdx(i)) );
    end
    fprintf( '\n' );
    S_h(cIdx) = 1e-6;
end
if garbageGrpFlag == 1
%     tmp = lpi(end);
%     lpi(1:end-1) = log(sum(q_h(1:end-1, :), 2)+1e-6) - log( sum(sum(q_h(1:end-1, :))) ) + log(1-exp(tmp));
%     lpi(end) = tmp;
    
else
    lpi = log(sum(q_h, 2)+1e-32) - log( N_t_1 );
end
lga = log(sum(q_m, 2)+1e-16) - log(N_t_1);
toc;
end

function params = checkParams(params)
    if isfield( params, 'SMALL_MEMORY_FLAG' ) == 0
        params.SMALL_MEMORY_FLAG = 1;
    end
    if isfield( params, 'OUT_FOLDER' ) == 0
        params.OUT_FOLDER = pwd;
    end
    if isfield( params, 'ITER_NUM' ) == 0
        params.ITER_NUM = 100;
    end
    if isfield( params, 'ADMM_IT_NUM' ) == 0
        params.ADMM_IT_NUM = 200;
    end
    if isfield( params, 'gProb' ) == 0
        params.gProb = 1-1e-14;
    end
    if isfield( params, 'rVal' ) == 0
        params.rVal = 1e-4;
    end
    if isfield( params, 'garbageGrpFlag' ) == 0
        params.garbageGrpFlag = 1;
    end
    if isfield( params, 'USE_GPU_FLAG' ) == 0
        params.USE_GPU_FLAG = 0;
    end
end