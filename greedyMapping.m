function [ mapping, diff, usedsG ] = greedyMapping( fG, sG, one2oneFlag )
%greedyMapping return a mapping from fG to sG.
%mapping is a matrix of size [size(fG,1), 2]
%diff is a vector of size [size(fG,1)]
%usedsG is a zero-one vector of size [size(sG,1)]
usedsG = zeros( size(sG, 1), 1 );
mapping = zeros( size(fG, 1), 2 );
cnt2 = 1;
diff = zeros( size(fG, 1), 1 );
for i = 1:length(fG)
%     fprintf( 'processing %d\n', i );
    dist = bsxfun( @minus, fG(i, :), sG );
    dist = sum( dist.^2, 2).^0.5;
    [~,idx] = sort(dist);
    cnt = 1;
    if one2oneFlag == 1
        while( usedsG(idx(cnt)) ~= 0 )
            cnt = cnt + 1;
        end
    end
    usedsG(idx(cnt)) = 1;
    mapping(cnt2, :) = [i idx(cnt)]; diff(cnt2) = norm(fG(i, :) - sG(idx(cnt), :) ); cnt2 = cnt2 + 1;
end

end

