function [CD4_T_Data, dataCell] = loadData( INPUT_FNAME, savePath )
%loadData load raw data from the csv file and convert it into *.mat files.
% Arguments:
%   INPUT_FNAME: should be full file path of the csv files.
%   savePath: if empty [], no saving occurred; otherwise save to the
%   savePath.
% Returns:
%   CD4_T_Data: The data cell samples in the order of inputs. A data
%   structure with the fields:
%       rawDataMat: the raw data matrix read from INPUT_FNAME
%       dataMat: the data matrix that might be preprocessed.
%       fieldName: the column name of the csv file (feature names)
%   dataCell: a cell of size [4 2]. The row corresponds to time slice (0,
%   1, 4, 12), and the second column is just the annotations.

%% loading data
% check if the file exist
assert( exist( INPUT_FNAME, 'file' ) == 2 );
fp = fopen( INPUT_FNAME, 'r' );
split = @(x) strsplit(x,',','CollapseDelimiter',false);
fieldName = split(fgetl(fp));
% rowNum = 0;
% while ~feof(fp)
%     row = str2double( split(fgetl(fp)) );
%     rowNum = rowNum + 1;
%     %dataCel = [dataCel; row];
% end
% fclose(fp);
% dataMat = zeros(1, length(fieldName) );
% fp = fopen( INPUT_FNAME, 'r' ); fgetl(fp);
% cnt = 1;
% dispstat('','init')
% while ~feof(fp)
% %     fprintf( ['read sample ' num2str(cnt) '. ' ] );
%     dispstat( ['read sample ' num2str(cnt) '... ' ] );
%     dataMat(cnt, :) = str2double( split(fgetl(fp)) ); cnt = cnt + 1;
% %     if mod(cnt, 1000) == 0
% %         fprintf( '\n' );
% %     end
% end
fclose(fp);
fprintf( 'reading file %s...\n', INPUT_FNAME)
dataMat = csvread(INPUT_FNAME, 1, 0);
fprintf( 'the file has %d rows and %d cols.\n', size(dataMat, 1), size(dataMat, 2) );
fprintf( 'finish reading!\n' );
CD4_T_Data.dataMat = dataMat; CD4_T_Data.fieldName = fieldName;
CD4_T_Data.rawDataMat = dataMat;
% BOX_FLAG = 0;
% [CD4_T_Data.dataMat, CD4_T_Data.centerVec, CD4_T_Data.rangeVec]...
%     = standardize(CD4_T_Data.dataMat, BOX_FLAG, standardizeMode);
%% extract different groups
timeVec = unique(CD4_T_Data.dataMat(:,1))';

dataCell = cell( length(timeVec), 2 );
cnt = 1;
for i = timeVec
    dataCell{cnt, 1} = CD4_T_Data.dataMat( CD4_T_Data.dataMat(:,1) == i, 2:end); dataCell{cnt, 2} = num2str(i);
    cnt = cnt + 1;
end

if ~isempty( savePath )
    [~,name,~] = fileparts(INPUT_FNAME);
    save( [ savePath filesep name '.mat'], 'CD4_T_Data', 'dataCell', '-v7.3');
end
%M = csvread('130_CD4_T_cells.csv');

end

