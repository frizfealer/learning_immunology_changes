function [x, centerVec, rangeVec, quan95Vec] = standardize(x,useBoxCox, standardizeMode)
if nargin<2
    useBoxCox = 0;
end
tostandardize = zeros(size(x,2),1);
for i=1:size(x,2)
    vec = x(:,i);
    u = unique(vec);
    if length(u)>10
        if useBoxCox
            bcvec = boxcox(vec - min(vec) + 0.1);
            x(:,i) = bcvec;
        end
        tostandardize(i) = 1;    
    end
end

sz = [size(x,1) 1];
subx = x(:,tostandardize>0);
if standardizeMode == 0 %with mean and standard deviation
    centerVec = nanmean(subx,1);
    subx = subx - repmat(centerVec,sz);
    rangeVec = nanstd(subx,1);
    subx = subx./repmat(rangeVec,sz);
    quan95Vec = nan;
elseif standardizeMode == 1 %with mean and range
    centerVec = nanmean(subx,1);
    subx = subx - repmat(centerVec,sz);
    rangeVec = zeros( 1, size(subx, 2) );
    for i = 1:size(subx, 2)
        rangeVec(i) = max(subx(:, i)) - min(subx(:, i));
    end
    subx = subx./repmat(rangeVec, sz);
    quan95Vec = nan;
elseif standardizeMode == 2 %quantile normalization
    centerVec = median(subx,1);
    subx = subx - repmat(centerVec,sz);
    rangeVec = zeros( 1, size(subx, 2) );
    for i = 1:size(subx, 2)
        rangeVec(i) = quantile(subx(:, i), 0.95) - quantile(subx(:, i), 0.05);
    end
    subx = subx./repmat(rangeVec, sz);
    quan95Vec = nan;
elseif standardizeMode == 3 
%quantile normalization on nonzero values, because they might be too many zero values,
%did >0.95 suppression for outlier.
    centerVec = zeros( 1, size(subx, 2) );
    rangeVec = zeros( 1, size(subx, 2) );
    quan95Vec = zeros( 1, size(subx, 2) );
    for i = 1:size(subx, 2)
        cNonZIdx = find( subx(:, i)~=0 );
        centerVec(i) = nanmedian( subx(cNonZIdx, i) );
        quan95Vec(i) = quantile( subx(cNonZIdx, i), 0.95 );
        samIdx =  subx(:, i) > quan95Vec(i) ;
%         fprintf( '%.3g %.3g %.1g%%\n', quan95, 0.01*centerVec(i), 100*( 0.01*centerVec(i)/ quan95 ) );
        subx(samIdx, i) = quan95Vec(i) + 0.01*centerVec(i);
        rangeVec(i) = quan95Vec(i) - quantile( subx(cNonZIdx, i), 0.05 );
        subx(:, i) = ( subx(:, i) - centerVec(i) ) / rangeVec(i);
    end
end
x(:,tostandardize>0) = subx;