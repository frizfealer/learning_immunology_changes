There are three scripts for usage.
1. script_processing_allData is about loading data and preprcoessing and saving.
2. script_grad_analysis is about running the gradient analysis.
3. script_result_analysis is analyzing the result. 
	Since I am not sure what kind of analysis are going to do,
	Here I show the most important one: computing posterior. 
	With this code, you can compute the probability of a cell using a specfic group of gradient, 
	and the probability of a cell matching to a specific cell in the previous time step.

Update 20170529
1. script_analysis_20170525 run the analysis from the preprocessing to compute gradient clustering and usage.
Generate training and testing matrix that have gradient usage proportion as features.