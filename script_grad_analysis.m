%% load data
load( 'aCD4_T_Data_norm_p_20170524.mat' );

%% setting up parallel
matlabpool open 64

%% run ADMM
FILE_NAME = 'aP_4_gn_25_tmp.mat';
params = [];
gradGrpNum = 25; %selected by 1000 samples
params.gProb = 1-1e-8;
gData = [];

rng('default');
cIdx = randperm( size(aDataCell{1,1}, 1) ); cIdx = cIdx(1:size(aDataCell{1,1}, 1)/2);
aDataCell{1, 1} =  aDataCell{1,1}(cIdx, :);
cIdx = randperm( size(aDataCell{1,2}, 1) ); cIdx = cIdx(1:round(size(aDataCell{1,2}, 1)/2));
aDataCell{1, 2} =  aDataCell{1,2}(cIdx, :); aDataCell{2, 1} = aDataCell{1, 2};
cIdx = randperm( size(aDataCell{2,2}, 1) ); cIdx = cIdx(1:round(size(aDataCell{2,2}, 1)/2));
aDataCell{2, 2} =  aDataCell{2,2}(cIdx, :);

aDataCellTrain = aDataCell([1, 2, 3, 4, 5, 6, 15, 16, 17, 18], :);
aDataCellTest = aDataCell([7, 8, 9, 10, 11, 12, 13, 14], :);
[ resStrucCel, llCel, g_pri, S_pri, uCel, rVec, sVec, rho ] = EM_v1_1_5_7_ADMM( aDataCellTrain, gradGrpNum , gData, FILE_NAME, params);

save( [FILE_NAME '_res.mat'], 'resStrucCel', 'llCel', 'g_pri', 'uCel', 'rVec', 'sVec', 'rho', '-v7.3' );

