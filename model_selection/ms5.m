%% setting flag
CSBIO = 0;
PCT = 0;
gradGrpNum = 30;
%% get z from the filename
aaa = mfilename;
toks  = regexp( aaa, 'ms([0-9]+)', 'tokens' );
x = str2num(toks{1}{1});
%% setting path
if CSBIO == 1
    addpath( '~/CD4_data_2' );
else
    addpath( '~/CD4_test' );
end
%% load data
load( 'aCD4_T_Data_norm_p_20170524.mat' );
%% setting up parallel
if CSBIO == 1
    parpool( 'local', 12 );
else
    if PCT == 1
        matlabpool open local 2
    else
        matlabpool open 2
    end
end
%% choosing a subset of data.
rng('default');
TRAIN_NUM = 1000;
VALID_NUM = 1000;
trainDCell = cell( size( aDataCell ) );
validDCell = cell( size( aDataCell ) );
cnt = 1;
for i = 1:size(aDataCell, 1)
    s1 = size( aDataCell{i, 1}, 1 );
    s2 = size( aDataCell{i, 2}, 1 );
    if s1 >= TRAIN_NUM+VALID_NUM && s2 >= TRAIN_NUM+VALID_NUM
        cIdx = randperm( s1, (TRAIN_NUM+VALID_NUM) );
        trainDCell{cnt, 1} = aDataCell{i, 1}(cIdx(1:TRAIN_NUM), :);
        validDCell{cnt, 1} = aDataCell{i, 1}(cIdx(TRAIN_NUM+1:end), :);
        cIdx = randperm( s2, (TRAIN_NUM+VALID_NUM) );
        trainDCell{cnt, 2} = aDataCell{i, 2}(cIdx(1:TRAIN_NUM), :);
        validDCell{cnt, 2} = aDataCell{i, 2}(cIdx(TRAIN_NUM+1:end), :);
        cnt = cnt + 1;
    end
end
for i = size(aDataCell, 1):-1:1
    if isempty(trainDCell{i, 1})
        break;
    end
end
trainDCell = trainDCell(1:i-1,:);
validDCell = validDCell(1:i-1,:);

%% run ADMM with different gradient group number
FILE_NAME = sprintf( 'aP_model_select_%d.mat', gradGrpNum );
params = [];
gData = [];
params.gProb = 1-1e-9;
[ resStrucCel, llCel, g_pri, S_pri, uCel, rVec, sVec, rho ] = EM_v1_1_5_7_ADMM( trainDCell, gradGrpNum , gData, FILE_NAME, params);

llAry = zeros(size(validDCell, 1),1);
for i = 1:size(validDCell, 1)
    tStart = tic;
    fprintf( 'processing experiment %d...', i );
    garbageGrpFlag = 1;
    x_t = validDCell{i, 1}';
    x_t_1 = validDCell{i, 2}';
    cStruc = resStrucCel{i};
    cStruc.g_h = g_pri;
    cStruc.S_h = S_pri;
    [llAry(i), ~, ~, ~] = EM_E_step_ind( x_t, x_t_1, cStruc, garbageGrpFlag);
end
save( FILE_NAME, 'llAry');