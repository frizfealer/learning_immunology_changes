import fnmatch
import os
import sys
import subprocess
import re
import time
#inPath = str(sys.argv[1]);
#print 'Input path folder is: %s' % (inPath)
#os.chdir(inPath)
for root, dirnames, filenames in os.walk(os.getcwd()):
	print 'Get all files in this folder...'
[tmp, folderName] = os.path.split(os.getcwd())
print folderName
logFolderName = folderName+'_logs'
logFNPath = '../'+logFolderName+'/'
if not os.path.exists(logFNPath):
    os.makedirs(logFNPath)
for file in filenames:
	#cmd = 'matlab -nodesktop -nodisplay -nosplash -r "' + file[:-2] + '; exit" -logfile ' + file[:-2]+'.log &'
	#cmd = 'bsub -q week matlab -nodisplay -nosplash -singleCompThread -r ' + file[:-2];
	cmd = 'bsub -q day -n 1  matlab -nodisplay -nosplash -singleCompThread -r ' + file[:-2] + ' -logfile ' + file[:-2] + '.out'
	#cmd = 'bsub -q day -M 2 -n 12 -R "span[hosts=1]" matlab -nodisplay -nosplash -singleCompThread -r ' + file[:-2] + ' -logfile ' + file[:-2] + '.out'
	#print cmd
	#subprocess.call(cmd, shell=False)
	#subprocess.Popen(cmd, cwd = inPath);
	os.system( cmd )
	time.sleep(2)
