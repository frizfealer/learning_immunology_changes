%% load all data
load( 'aCD4_T_Data_norm_p.mat' );
%% load the result
load( 'aP_4_gn_45_it_100_norm_p.mat' );
%% set params.
gradNum = 47;
expNum = 15;
%% compute posterior and save
outFolder = 'pos_folder_norm_p';
mkdir( outFolder );
for i = 1:expNum
    tStart = tic;
    fprintf( 'processing experiment %d...', i );
    garbageGrpFlag = 1;
    x_t = aDataCell{i, 1}';
    x_t_1 = aDataCell{i, 2}';
    cStruc = resStrucCel{i};
    cStruc.g_h = g_pri;
    cStruc.S_h = S_pri;
    [~, ~, q_h, ~] = EM_E_step_ind( x_t, x_t_1, cStruc, garbageGrpFlag);
    save( sprintf( '%s/exp%d.mat', outFolder, i ), 'q_h' );
    tEnd = toc(tStart);
    fprintf( 'cost %.2g s.\n', tEnd );
end

%% compute posterior for q_m and save
outFolder = 'pos_folder_norm_p';
mkdir( outFolder );
for i = 1:expNum
    tStart = tic;
    fprintf( 'processing experiment %d...', i );
    garbageGrpFlag = 1;
    x_t = aDataCell{i, 1}';
    x_t_1 = aDataCell{i, 2}';
    cStruc = resStrucCel{i};
    cStruc.g_h = g_pri;
    cStruc.S_h = S_pri;
    [~, ~, ~, q_m] = EM_E_step_ind( x_t, x_t_1, cStruc, garbageGrpFlag);
    save( sprintf( '%s/exp%d_q_m.mat', outFolder, i ), 'q_m' );
    tEnd = toc(tStart);
    fprintf( 'cost %.2g s.\n', tEnd );
end

%% compute count table: 
%generate cntTab for later use
posteriorFolder = 'F:\CD4_project\data\pos_folder_norm_gn_45_norm_p'; %the folder of posterior files
load( 'aP_4_gn_45_it_100_norm_p.mat' ); %model file
load( 'aCD4_T_Data_norm_p.mat' ); %data file, load for feature name
expNum = 15; gradNum = 45+2; %expNum: 3*5 patients, gradNum: number of gradients

cntTab = zeros(expNum, gradNum);
shTab = zeros(expNum, gradNum);
priDiffTab = zeros(expNum, gradNum);
for i = 1:expNum
    load( [posteriorFolder '\' sprintf( 'exp%d.mat', i )] );
    ins2 = sum(q_h, 2);
    for j = 1:gradNum
        shTab(i, j) = resStrucCel{i}.S_h(j);
        priDiffTab(i, j) = norm( g_pri(:, j) - resStrucCel{i}.g_h(:, j) );
    end
    cntTab(i, :) = ins2 / sum(ins2);
end
mask = cntTab > 0.01;
shTab = shTab.*mask;
shTab(:, end) = 0;
priDiff = priDiffTab.*mask;
% figure; subplot(2, 2, 1); imagesc(cntTab); colorbar; ylabel( 'patient' ); xlabel( 'gradient group' ); title( 'count table' );
% subplot(2, 2, 2); imagesc(shTab);  colorbar; ylabel( 'patient' ); xlabel( 'gradient group' ); title( 'variance' );
% subplot(2, 2, 3); imagesc(priDiffTab); colorbar;
oCntTab = cntTab;

%% kmeans clustering on the gradient, 
%modify cntTab, output gradOrd, tIdx (gradient index for used gradient)
%extract used gradients
tIdx = sum(cntTab, 1); tIdx = find(tIdx > 0.01 );
tIdx(tIdx == gradNum) = [];

% try k from 1 to 20
rng( 'default' );
cntTab = cntTab(:, tIdx);
opts = statset('Display','off');
KVec = zeros(1, 20);
for i = 1:length(KVec)
    [idx,C, sumD] = kmeans( cntTab',i, ...
    'Replicates',100,'Options',opts);
    KVec(i) = sum(sumD);
end
figure; plot(KVec);
rng( 'default' );
[idx,C, sumD] = kmeans( cntTab', 12, ... %choose 12 by elbow method.
    'Replicates',100,'Options',opts);
[~, ind1] = sort(idx);
cntTab = cntTab(:, ind1);
figure; imagesc(cntTab');
gradOrd = ind1;

%% kmeans clustering on the patient (after on gradients)
%modify cntTab, output patientOrd
% try k from 1 to 20
rng( 'default' );
opts = statset('Display','off');
KVec = zeros(1, 15);
for i = 1:length(KVec)
    [idx,C, sumD] = kmeans( cntTab,i, ...
    'Replicates',100,'Options',opts);
    KVec(i) = sum(sumD);
end
figure; plot(KVec);
rng( 'default' );
[idx, ~, ~] = kmeans( cntTab,10, ... %choose 10 by elbow method.
    'Replicates',200,'Options',opts);
[~, ind2] = sort(idx);
cntTab = cntTab(ind2, :);
figure; imagesc(cntTab'); set( gca, 'xtick', 1:15 ); set( gca, 'xticklabel', num2str(ind2) );
patientOrd = ind2;
%save the cntTab and the biclustering order
save( 'grad_45_norm_p_cnt_tab.mat', 'oCntTab', 'cntTab', 'gradOrd', 'patientOrd' );

%% gnerate summary figure
%generate label pTab
pTab = cell(15, 1);
pTmp = [118 119 125 129 130];
tTmp = {'0-1', '1-4', '4-12'};
cnt = 1;
for i = 1:5
    for j = 1:3
        pTab{cnt} = sprintf( '%d:%s', pTmp(i), tTmp{j} );
        cnt = cnt + 1;
    end
end

%basic ploting
figure; set(gcf, 'position', [1 41 1366 651] ); 
%generate left half image, the heatmap for count table
ca(1) = subplot(1, 2, 1); 
imagesc(cntTab'); set( gca, 'xtick', 1:15 ); set( gca, 'xticklabel', pTab(patientOrd) ); 
set( gca, 'xtickLabelRotation', 90 ); fCBHan=colorbar('westoutside');
aaa = colormap( gca, gray );
colormap( gca, aaa(end:-1:1,:) );
ylabelHan = ylabel( 'gradient' ); title( 'Gradient usage for each nearby time points' );
set( gca, 'fontweight', 'bold', 'fontsize', 12 );
tmp = g_pri(:, tIdx); maxV = max(tmp(:)); minV = min(tmp(:)); maxminV = max( maxV, abs(minV) );
if maxminV == maxV
    otherSideV = -maxV;
else
    otherSideV = -minV;
end
%padding the column to make the max min 's magnitiude the same
%generate the right half image, the heatmap for the learned gradient
ca(2) = subplot(1, 2, 2); imagesc([g_pri(:, tIdx(gradOrd))' ones(length(tIdx), 1)*otherSideV]); 
cb = colorbar; %tmp = get(cb, 'xtick'); tmp(end) = maxV; set(cb, 'xtick', tmp);
set( gca, 'xtick', 1:21); set( gca, 'xticklabel', aCD4_T_Data.fieldName(2:end) ); set( gca, 'xtickLabelRotation', 90 );
set(gca, 'ytick', (1:length(tIdx))+0.5); set(gca, 'yticklabel', [] ); title( 'Gradient details');
set( gca, 'xlim', [0.5 21.5]);
tmp = colormap( gca, redblue );
set( gca, 'fontweight', 'bold', 'fontsize', 12 );
% [cmin,cmax] = caxis; map = colormap(redblue);
linkaxes(ca, 'y')

% manage heatmap style for count table
set( ca(1), 'xtick', (1:15)+0.5);
set( ca(1), 'xticklabel', [] );
set( ca(1), 'xgrid', 'on' );
xTextVec = [];
axes(ca(1));
for i = 1:expNum
    xTextVec(i) = text( i, length(tIdx)+1, pTab(ind2(i)));
    set(xTextVec(i), 'Rotation', 90 );
    set(xTextVec(i), 'FontSize', 12, 'Fontweight', 'bold' );
    tmp = get(xTextVec(i), 'Extent' );
    set(xTextVec(i), 'Position', [i tmp(2)+tmp(4) 0]);
end
set( ca(1), 'ytick', (1:length(tIdx))+0.5);
set( ca(1), 'yticklabel', [] );
set( ca(1), 'ygrid', 'on' );
yTextVec = [];
axes(ca(1));
for i = 1:length(tIdx)
    yTextVec(i) = text( 0, i, num2str(i));
    set(yTextVec(i), 'FontSize', 10, 'Fontweight', 'bold' );
    tmp = get(yTextVec(i), 'Extent' );
    if i >= 10
        set(yTextVec(i), 'Position', [0-0.2 i 0]);
    end
end
set(ylabelHan, 'Position', [-0.5 26.5 1]);
set(fCBHan, 'position',[0.065    0.1167    0.0195    0.8080]);

% modify heatmap style for gradient table
set( ca(2), 'xtick', (1:21)+0.5);
set( ca(2), 'xticklabel', [] );
set( ca(2), 'xgrid', 'on' );
xTextVec2 = [];
axes(ca(2));
for i = 1:21
    xTextVec2(i) = text( i, length(tIdx)+1, aCD4_T_Data.fieldName(i+1));
    set(xTextVec2(i), 'Rotation', 90 );
    set(xTextVec2(i), 'FontSize', 12, 'Fontweight', 'bold' );
    tmp = get(xTextVec2(i), 'Extent' );
    set(xTextVec2(i), 'Position', [i tmp(2)+tmp(4) 0]);
end
set( ca(2), 'ygrid', 'on' );

% merge twos heatmap yaxis
pos1 = get( ca(1), 'Position' );
pos2 = get( ca(2), 'position' );
pos2(1) = pos1(1)+pos1(3);
set( ca(2), 'position', pos2);
pos1 = get( ca(1), 'Position' );
pos2 = get( ca(2), 'position' );
pos2(2) = pos1(2);
set(ca(2), 'position', pos2);
pos1 = get( ca(1), 'Position' );
pos2 = get( ca(2), 'position' );
pos2(4) = pos1(4);
set(ca(2), 'position', pos2);

%output file
set(gcf, 'PaperPositionMode', 'auto');
set(gcf,'PaperType', 'USletter');
set(gcf,'PaperOrientation', 'Landscape');
pp = get(gcf, 'PaperPosition' ); 
pp(1) = -0.5;
set(gcf,'PaperPositionMode', 'manual', ...
    'PaperUnits','inches', ...
    'Paperposition', pp); 
print('gradient_5_patients_bicluster_summary_grad_num_45_norm_each_patient.pdf','-dpdf')

