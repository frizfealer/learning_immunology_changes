# README #

## What is this repository for? ##

This repository is the codes for the project: "Learning the changes in human immunology development from T-Cell markers data."

## dependency ##
This work is built on MATLAB

## description ##
1. Please refer to "EM_v1.1.pdf" for the definition of our model.
2. Please refer to Readme.txt for basic usages.
