%% load data from raw (*.csv), preprocessing based on each patient, and save the *.mat file.

pID = [118 119 125 129 130]; %patient ID

%In this case each csv file is in the location:
%F:\CD4_project\data\p[ID]\[ID]_T_cells.csv, where ID is the pID
for i = 1:length(pID)
    cFPath = sprintf( 'F:\\CD4_project\\data\\p%d\\%d_CD4_T_cells.csv', pID(i), pID(i) );
    savePath = sprintf( 'F:\\CD4_project\\data\\p%d\\%d_CD4_T_cells.mat', pID(i), pID(i) );
    [CD4_T_Data, ~] = loadData( cFPath, [] );
    [ CD4_T_Data, dataCell ] = preprocessing( CD4_T_Data );
    save( savePath, 'CD4_T_Data', 'dataCell' );
end

%% using the immune measurements only appears in p130 
%(it includes 22 measurements,as the first field being time slice, and the 21 meausrements later on.)
load( 'F:\\CD4_project\\data\\p130\\130_CD4_T_cells.mat' );
targetMarkers = CD4_T_Data.fieldName;
for i = 1:length(pID)-1
    cFPath = sprintf( 'F:\\CD4_project\\data\\p%d\\%d_CD4_T_cells.mat', pID(i), pID(i) );
    load( cFPath );
    [CD4_T_Data, dataCell] = retainFeatures( CD4_T_Data, targetMarkers );
    save( cFPath, 'CD4_T_Data', 'dataCell' );
end


%% processing all data, combine the data from five patients
pID = [118 119 125 129 130];
aCD4_T_Data = struct( 'rawDataMat', [], 'dataMat', [], 'centerVecCel', [], ...
    'rangeVecCel', [], 'fieldName', [], 'pRange', [] );
aDataCell = [];
cnt = 1;
cnt2 = 1;
for i = 1:length(pID)
    cFPath = sprintf( 'F:\\CD4_project\\data\\p%d\\%d_CD4_T_cells.mat', pID(i), pID(i) );
    load( cFPath );
    aCD4_T_Data.rawDataMat = [aCD4_T_Data.rawDataMat; CD4_T_Data.rawDataMat];
    aCD4_T_Data.dataMat = [aCD4_T_Data.dataMat; CD4_T_Data.dataMat];
    aCD4_T_Data.centerVecCel{i} = CD4_T_Data.centerVec;
    aCD4_T_Data.rangeVecCel{i} = CD4_T_Data.rangeVec;
    aCD4_T_Data.fieldName = CD4_T_Data.fieldName;
    aCD4_T_Data.pRange{i, 1} = [cnt cnt+size(CD4_T_Data.dataMat, 1)-1]; %a mapping from patient i to index range in dataMat and rawDataMat
    aCD4_T_Data.pRange{i, 2} = [pID(i)];
    %Construct the data structure that is used for analyzing.
    %it starts from pID = 118, and each row corresponds to two consecutive
    %time point. So the first three rows are:
    % data from p118 t0, data from p118 t1
    % data from p118 t1, data from p118 t2
    % data from p118 t2, data from p118 t3
    %Since we have four time point for each patient, we have three rows per
    %patient. Totally there are 3*5 rows.
    for j = 1:3 
        aDataCell{cnt2, 1} = dataCell{j, 1};
        aDataCell{cnt2, 2} = dataCell{j+1, 1};
        cnt2 = cnt2 + 1;
    end
    cnt = cnt+size(CD4_T_Data.dataMat, 1);
end
save( 'F:\\CD4_project\\data\\aCD4_T_Data_norm_p.mat', 'aCD4_T_Data', 'aDataCell' );