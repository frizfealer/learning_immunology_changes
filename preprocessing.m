function [ CD4_T_Data, dataCell ] = preprocessing( CD4_T_Data, sMode )
%preprocessing preprocessing using the standardize method.
% Arguments:
%   CD4_T_Data: the data structure comes from loadData.
%   dataCell: same as the above.
% Returns:
%   CD4_T_Data: updated CD4_T_Data. has the following property:
%       rawDataMat: the raw data matrix read from INPUT_FNAME
%       dataMat: is the preprocessed data matrix.
%       fieldName: the column name of the csv file (feature names)
%       centerVec: the center of each measurement.
%       rangeVec: the rangeVec (denominator) of each measurement.
%   dataCell: same as the above. Updated with dataMat

if nargin < 2
    standardizeMode =  3;
elseif nargin == 2
    standardizeMode = sMode;
end
 

BOX_FLAG = 0;
%standardizeMode = 3; %quantile normalization with suppression.
[CD4_T_Data.dataMat, CD4_T_Data.centerVec, CD4_T_Data.rangeVec]...
    = standardize(CD4_T_Data.rawDataMat, BOX_FLAG, standardizeMode);
%% extract different groups
timeVec = unique(CD4_T_Data.dataMat(:,1))';
dataCell = cell( length(timeVec), 2 );
cnt = 1;
for i = timeVec
    dataCell{cnt, 1} = CD4_T_Data.dataMat( CD4_T_Data.dataMat(:,1) == i, 2:end); dataCell{cnt, 2} = num2str(i);
    cnt = cnt + 1;
end

end

