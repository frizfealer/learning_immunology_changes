%% load data from raw (*.csv), preprocessing based on each patient, and save the *.mat file.

%patient ID
pID = [135, 136, 137, 138, 139, 510, 513, 514, 515];

for i = 1:length(pID)
    cFPath = sprintf( 'C:/Users/DUKE3/gradient_analysis/data/%d_CD4pos_T_cells.csv', pID(i) );
    savePath = sprintf( 'C:/Users/DUKE3/gradient_analysis/data/%d_CD4_T_cells.mat', pID(i) );
    [CD4_T_Data, ~] = loadData( cFPath, [] );
    [ CD4_T_Data, dataCell ] = preprocessing( CD4_T_Data );
    save( savePath, 'CD4_T_Data', 'dataCell' );
end

%% processing all data, combine the data from five patients
aCD4_T_Data = struct( 'rawDataMat', [], 'dataMat', [], 'centerVecCel', [], ...
    'rangeVecCel', [], 'fieldName', [], 'pRange', [] );
aDataCell = [];
cnt = 1;
cnt2 = 1;
for i = 1:length(pID)
    cFPath = sprintf( 'C:/Users/DUKE3/gradient_analysis/data/%d_CD4_T_cells.mat', pID(i) );
    load( cFPath );
    aCD4_T_Data.rawDataMat = [aCD4_T_Data.rawDataMat; CD4_T_Data.rawDataMat];
    aCD4_T_Data.dataMat = [aCD4_T_Data.dataMat; CD4_T_Data.dataMat];
    aCD4_T_Data.centerVecCel{i} = CD4_T_Data.centerVec;
    aCD4_T_Data.rangeVecCel{i} = CD4_T_Data.rangeVec;
    aCD4_T_Data.fieldName = CD4_T_Data.fieldName;
    aCD4_T_Data.pRange{i, 1} = [cnt cnt+size(CD4_T_Data.dataMat, 1)-1]; %a mapping from patient i to index range in dataMat and rawDataMat
    aCD4_T_Data.pRange{i, 2} = [pID(i)];
    %Construct the data structure that is used for analyzing.
    %it starts from pID = 118, and each row corresponds to two consecutive
    %time point. So the first three rows are:
    % data from p118 t0, data from p118 t1
    % data from p118 t1, data from p118 t2
    % data from p118 t2, data from p118 t3
    %Since we have four time point for each patient, we have three rows per
    %patient. Totally there are 3*5 rows.
    for j = 1:length(dataCell)-1 
        aDataCell{cnt2, 1} = dataCell{j, 1};
        aDataCell{cnt2, 2} = dataCell{j+1, 1};
        cnt2 = cnt2 + 1;
    end
    cnt = cnt+size(CD4_T_Data.dataMat, 1);
end
save( 'C:/Users/DUKE3/gradient_analysis/data/aCD4_T_Data_norm_p.mat', 'aCD4_T_Data', 'aDataCell' );

%% load data
load( '../data/aCD4_T_Data_norm_p_20170524.mat' );
rng('default');
%choose subset of cell samples to increase speed
cIdx = randperm( size(aDataCell{1,1}, 1) ); cIdx = cIdx(1:size(aDataCell{1,1}, 1)/2);
aDataCell{1, 1} =  aDataCell{1,1}(cIdx, :);
cIdx = randperm( size(aDataCell{1,2}, 1) ); cIdx = cIdx(1:round(size(aDataCell{1,2}, 1)/2));
aDataCell{1, 2} =  aDataCell{1,2}(cIdx, :); aDataCell{2, 1} = aDataCell{1, 2};
cIdx = randperm( size(aDataCell{2,2}, 1) ); cIdx = cIdx(1:round(size(aDataCell{2,2}, 1)/2));
aDataCell{2, 2} =  aDataCell{2,2}(cIdx, :);
%set training/testing set
% aDataCellTrain = aDataCell([1, 2, 3, 4, 5, 6, 15, 16, 17, 18], :);
% aDataCellTest = aDataCell([7, 8, 9, 10, 11, 12, 13, 14], :);
%t0 to t1 only
aDataCellTrain = aDataCell([1, 3, 5, 15, 17], :);
aDataCellTest = aDataCell([7, 9, 11, 13], :);

%% run ADMM, usually run on the cluster
FILE_NAME = 'aP_train_gn_20_t0t1_res';
params = [];
gradGrpNum = 25; %selected by 1000 samples
params.gProb = 1-1e-8;
gData = [];
[ resStrucCel, llCel, g_pri, S_pri, uCel, rVec, sVec, rho ] = EM_v1_1_5_7_ADMM( aDataCellTrain, gradGrpNum , gData, FILE_NAME, params);
save( [FILE_NAME '_res.mat'], 'resStrucCel', 'llCel', 'g_pri', 'S_pri', 'uCel', 'rVec', 'sVec', 'rho', '-v7.3' );

%% compute posterior and normalize across patient/time
%load('aP_train_gn_25_.mat');
load( FILE_NAME );
qmFlag = 0;
%forget to return S_pri...
S_pri = zeros( size(resStrucCel{1}.S_h) );
for z = 1:length(uCel.var)
    udivideRho = bsxfun( @rdivide, uCel.var{z}, rho.var );
    S_pri = S_pri + 1./(resStrucCel{z}.S_h) + udivideRho;
end
S_pri = S_pri / length(uCel.var);
S_pri = 1./(S_pri); %back to variance

featureAryTrain = zeros(size(aDataCellTrain, 1), size(g_pri, 2)-2);
for i = 1:size(aDataCellTrain, 1)
    tStart = tic;
    fprintf( 'processing experiment %d...', i );
    garbageGrpFlag = 1;
    x_t = aDataCellTrain{i, 1}';
    x_t_1 = aDataCellTrain{i, 2}';
    cStruc = resStrucCel{i};
    cStruc.g_h = g_pri; %actually using global gradient and variance to compute posteriors
    cStruc.S_h = S_pri;
    cStruc.lga = -log(size(x_t, 2))*ones(size(x_t, 2),1);
    [~, ~, q_h, ~] = EM_E_step_ind( x_t, x_t_1, cStruc, garbageGrpFlag, qmFlag);
    tEnd = toc(tStart);
    featureAryTrain(i, :) = mean(q_h(1:size(g_pri, 2)-2, :), 2);
    fprintf( 'cost %.2g s.\n', tEnd );
end

featureAryTest = zeros(size(aDataCellTest, 1), size(g_pri, 2)-2);
for i = 1:size(aDataCellTest, 1)
    tStart = tic;
    fprintf( 'processing experiment %d...', i );
    garbageGrpFlag = 1;
    x_t = aDataCellTest{i, 1}';
    x_t_1 = aDataCellTest{i, 2}';
    cStruc = resStrucCel{i};
    cStruc.g_h = g_pri;
    cStruc.S_h = S_pri;
    cStruc.lga = -log(size(x_t, 2))*ones(size(x_t, 2),1);
    [~, ~, q_h, ~] = EM_E_step_ind( x_t, x_t_1, cStruc, garbageGrpFlag, qmFlag);
    tEnd = toc(tStart);
    featureAryTest(i, :) = mean(q_h(1:size(g_pri, 2)-2, :), 2);
    fprintf( 'cost %.2g s.\n', tEnd );
end

%using t0 to t1, t1 to t12
% trainFAry = zeros(size(aDataCellTrain, 1)/2, 2*(size(g_pri, 2)-2));
% for i = 1:2:size(aDataCellTrain, 1)
%     trainFAry(i, 1:(size(g_pri, 2)-2)) = featureAryTrain(i, :);
%     trainFAry(i, (size(g_pri, 2)-2):end) = featureAryTrain(i+1, :);
% end
% testFAry = zeros(size(aDataCellTest, 1)/2, 2*(size(g_pri, 2)-2));
% for i = 1:2:size(aDataCellTest, 1)
%     testFAry(i, 1:(size(g_pri, 2)-2)) = featureAryTest(i, :);
%     testFAry(i, (size(g_pri, 2)-2):end) = featureAryTest(i+1, :);
% end

%using only t0 to t1
trainFAry = featureAryTrain;
testFAry = featureAryTest;

save( [FILE_NAME '_fArys.mat'], 'trainFAry', 'testFAry');