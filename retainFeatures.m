function [CD4_T_Data, dataCell] = retainFeatures( CD4_T_Data, targetMarkers )
%retainFeatures retain only the markers specified in targetMarkers.
% Arguments:
%   CD4_T_Data: the data structure comes from loadData.
%   dataCell: same as the above.
% Returns:
%   CD4_T_Data: updated CD4_T_Data. has the following property:
%       dataMat: is the preprocessed data matrix.
%       fieldName: the column name of the csv file (feature names)
%       (centerVec): the center of each measurement.
%       (rangeVec): the rangeVec (denominator) of each measurement.
%   dataCell: same as the above. Updated with dataMat

%% For every element in targetMarkers, check if it is in the data
%targetIdx: in map from targetMarkers to index in CD4_T_Data
cFName = CD4_T_Data.fieldName;
targetIdx = ones(length(targetMarkers), 1)*-1;
for i = 1:length(targetMarkers)
    cIdx = -1;
    for j = 1:length(cFName)
        if strcmp( targetMarkers{i}, cFName{j} )  == 1 ...
                || strcmp( targetMarkers{i}(2:end), cFName{j} ) == 1 ...
                ||  strcmp( targetMarkers{i}, cFName{j}(2:end) ) == 1
            cIdx = j;
            break;
        end
    end
    if cIdx ~= -1
        targetIdx(i) = cIdx;
    end
end
targetIdx(targetIdx==-1) = [];
fprintf( '# features retained: %d\n', length(targetIdx) );
fields = fieldnames(CD4_T_Data);
for i = 1:numel(fields)
    if strcmp( fields{i}, 'fieldName' ) == 1
       CD4_T_Data.(fields{i}) = cFName(targetIdx);
    elseif isvector( CD4_T_Data.(fields{i}) ) %centerVec or rangeVec or quan95Vec
        CD4_T_Data.(fields{i}) = CD4_T_Data.(fields{i})(targetIdx(2:end)-1); %the time field is always matched, and always be the first element.
    else
        CD4_T_Data.(fields{i}) = CD4_T_Data.(fields{i})(:, targetIdx);
    end
end

%% update dataCell
dataCell = cell( 4, 2 );
dataCell{1, 1} = CD4_T_Data.dataMat( CD4_T_Data.dataMat(:,1) == 0, 2:end); dataCell{1, 2} = 'b00';
dataCell{2, 1} = CD4_T_Data.dataMat( CD4_T_Data.dataMat(:, 1) == 1, 2:end); dataCell{2, 2} = 'w01';
dataCell{3, 1} = CD4_T_Data.dataMat( CD4_T_Data.dataMat(:, 1) == 4, 2:end); dataCell{3, 2} = 'w04';
dataCell{4, 1}= CD4_T_Data.dataMat( CD4_T_Data.dataMat(:, 1) == 12, 2:end); dataCell{4, 2} = 'w12';

end

